var results = [];

var bar = "Groningen"
const http = require("http");
const {
    mergeWith,
    cloneDeep,
    isNull,
    isNumber,
    isBoolean,
    merge,
    result,
    add,
} = require("lodash");

const soaper = require("theendsoaper");
const Type = require("theendsoaper/modules/function-types");
const info = {
    "agendaId": "13358",
    "calendarAPICompany": "TheEndKaraoke",
    "calendarAPILocation": "Groningen",
    "cashReference": 5000000060,
    "database": "END-K1-101",
    "internetReference": 5000000075,
    "key": "gabh41gfmx38-gzzh00",
    "password": "Yh36@hJ$rIhiD6dF",
    "path": "http://82.217.205.103:3064/api/v1/",
    "pinReference": 5000000068,
    "salesAreaId": "500000311",
    "secret": "a69c8ad4be3f5f767cb0819a4dfbefadeca06b22",
    "staffReference": 5003647452,
    "token": "nw0IxN1EZczg",
    "url": "http://82.217.205.103:3063/soap/ITPAPIPOS",
    "username": "API",
    "vipHalf": "05000000936",
    "vipHour": "05000000938"
}
var https = require('https');

async function start() {
    var start = new Date(); //now
    var end = new Date(start);
    start.setHours(start.getHours() - 1); ///-1 for live

    //debug only
    start = new Date();
    start.setDate(24);
    start.setMonth(1);
    start.setFullYear(2024);
    start.setHours(1);
    start.setMinutes(0);
    start.setSeconds(0);

    end = new Date(start);
    end.setHours(end.getHours() + 1);


    results.push("Getting: " + bar + " for date: " + start.toDateString() + " to " + end.toDateString());

    var orders = await getValuesForBetweenDates(bar, info, start, end).then(a => a).catch(e => {
        console.error('Error getting ordeers: ', e);
        return null;
    });

    if (!orders) {
        results.push('no orders');
    }



    var items = [];




    for (var report of orders.actualPaidOrders) {
        for (var order of report.orders) {

            for (var item of order.items) {
                items.push({
                    time: new Date(order.dateTime).getTime(),
                    amount: numberTo2Decimals(item.singlePrice * item.quantity),
                    quantity: item.quantity,

                });
            }
        }
    }

    var lastHour = new Date(start);

    var last15Minutes = new Date(start);
    last15Minutes.setMinutes(last15Minutes.getMinutes() + 45);

    var last5Minutes = new Date(start);
    last5Minutes.setMinutes(last5Minutes.getMinutes() + 55);

    var middle5Minutes = new Date(start);
    middle5Minutes.setMinutes(middle5Minutes.getMinutes() + 50);

    var first5Minutes = new Date(start);
    first5Minutes.setMinutes(first5Minutes.getMinutes() + 45);

    var data = {
        barname: bar,
        lastHour: 0,
        last15Minutes: 0,
        last5Minutes: 0,
        middle5Minutes: 0,
        first5Minutes: 0,
        individualOrders: items.length,
        start: start.getTime(),
        end: end.getTime(),
        key: "DuncanSaundersVerySecretKeyAJSDHSAJKDSKAJHD"
    }

    for (var item of items) {
        if (item.time > lastHour.getTime()) {
            data.lastHour += item.amount;
        }

        if (item.time > last15Minutes.getTime()) {
            data.last15Minutes += item.amount;
        }

        if (item.time > last5Minutes.getTime()) {
            data.last5Minutes += item.amount;
        }

        if (item.time > middle5Minutes.getTime()) {
            data.middle5Minutes += item.amount;
        }

        if (item.time > first5Minutes.getTime()) {
            data.first5Minutes += item.amount;
        }

    }


    console.log(data);

    results.push(data);
    await sendToCustomerServer(data);
    return results;
}

function numberTo2Decimals(number) {
    return Math.round(Number(number) * 100) / 100;
}


async function getValuesForBetweenDates(bar, info, start, end) {
    if (!(info.cashReference &&
            info.pinReference &&
            info.internetReference &&
            info.url &&
            info.password &&
            info.username &&
            info.token)) {
        return TypeError('Info must have all its variables');
    }

    // console.log("Got Info for: ", bar, info);
    let cash = 0;
    let pin = 0;
    let extra = 0;
    let other = 0;
    let staffPin = [];
    var vats = [];
    var actualPaidOrders = [];
    var yesterday = new Date(start);
    var today = new Date(end);

    var tables = {};

    quantities = {
        cash: 0,
        pin: 0,
        internet: 0,
        staff: 0,
        other: 0
    }



    console.log("yesteday: " + yesterday.toISOString() + ", today: " + today.toISOString());

    return await soaper
        .detailedTurnoverReport(
            info.username,
            info.password,
            info.url,
            new Type().getDetailedTurnoverReport(),
            yesterday.toISOString().split("Z")[0],
            today.toISOString().split("Z")[0],
            info.token
        )
        .then((values2) => {


            if (Array.isArray(values2)) {

                values2.forEach((a) => {
                    if (a.bills.length === 0) {
                        // print.print(a.orders);
                    }
                    var table = String(a.tableNumber);
                    if (!tables[table]) {
                        tables[table] = {
                            name: String(a.tableNumber),
                            total: 0,
                            orders: 0,
                            opened: a.openDateTime,
                            closed: a.closeDateTime
                        }
                    }

                    tables[table].closed = a.closeDateTime;

                    a.orders.forEach((b) => {
                        //console.log(a);
                        //throw new Error('done');
                        b.items.forEach(o => {
                            var data = { drink: o.articleId, quantity: o.quantity, singlePrice: o.singlePrice, total: o.price, discount: o.discount, time: b.dateTime, vat: o.vatPercent, vatPaid: o.vat };
                            tables[table].orders += o.quantity;
                            vats.push(data);
                        })


                    })

                    a.bills.forEach((b) => {

                        b.payments.forEach((c) => {
                            tables[table].total += c.amount;
                            if (c.paymentId === info.pinReference) {
                                quantities.pin++;
                                pin += c.amount;
                                actualPaidOrders.push(a);
                            } else if (c.paymentId === info.cashReference) {
                                quantities.cash++;
                                cash += c.amount;
                                actualPaidOrders.push(a);
                            } else if (c.paymentId === info.internetReference) {
                                extra += c.amount;
                                quantities.internet++;
                                actualPaidOrders.push(a);
                            } else if (c.paymentId === info.staffReference) {
                                staffPin.push(a.orders);
                                quantities.staff++;
                            } else {
                                other += c.amount;
                                quantities.other++;
                                console.log("Payments that aren't defined: ", c.paymentId);
                            }
                        });
                    });



                });
            }

            return {

                data: {
                    pin: pin,
                    cash: cash,
                    extra: extra,
                    staffDrinks: staffPin,
                    quantities: quantities,

                    other: other,
                    yesterday: yesterday,
                    barname: bar,
                    tables: tables,
                },
                vats: vats,
                actualPaidOrders
            }
        }).catch((e) => {
            console.error(JSON.stringify(e));
            return new Error(e);
        })



}

async function sendToCustomerServer(data) {
    console.log("Sending to customer server: ", data);
    //url = "https://europe-west3-customerapp-e7bca.cloudfunctions.net/UpdateCreditPrice"
    var jsonData = JSON.stringify(data);
    const options = {
        hostname: 'europe-west3-customerapp-e7bca.cloudfunctions.net',
        path: '/UpdateCreditPrice',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': jsonData.length
        }
    };

    // Making the HTTP request
    const req = https.request(options, (res) => {
        let responseData = '';

        console.log(`statusCode: ${res.statusCode}`);

        res.on('data', (chunk) => {
            responseData += chunk;
        });

        res.on('end', () => {
            console.log('Response: ', JSON.parse(responseData));
        });

        res.on('error', (e) => {
            console.error('Error: ', e);
        });
    });

    req.on('error', (error) => {
        console.error(error);
    });

    // Writing the JSON data to the request body
    req.write(jsonData);

    // Ending the request
    req.end();


}

start();