/* 
C:\Users\dunca\Downloads\curl-win64-latest\curl-8.0.1_8-win64-mingw\bin\curl.exe   -X POST 'https://vpos-test.jforce.be/vpos/api/v1/payment' \
-H 'Content-Type: application/json' \
-H 'Authorization: Basic bF85QkNGQzZBMDYxRTc4ODA1NEZEODlGMUFCMzhBOEY4Qzg0QzIyNzYwRTZDRkM5Mzg6' \
-H 'Idempotency-Reference: 123e4567-e89b-12d3-a456-426655440000’
*/

exports.check = function (id) {
    const fetch = require('node-fetch');

    const key = "l_9BCFC6A061E788054FD89F1AB38A8F8C84C22760E6CFC938:";

    const basic = "Basic " + btoa(key);

    const url = 'https://vpos-test.jforce.be/vpos/api/v1/transaction'
    const options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': basic,
            'reference': id
        },
        method: 'GET',

    }

    console.log('checking: ' + id);
    fetch(url, options).then(res => res.json()).then(json => {

        var request;
        try {
            request = json.filter(a => a.reference == id)[0];
        } catch (e) {
            //request not found?
            console.log(json);
            console.error(e)
            return;
        }

        if (request.status == 'success') {
            console.log('payment successfull');
            return;
        } else {
            console.log('payment not successful', request.status)
        }

    })

}