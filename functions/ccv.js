const http = require("http");
const {
    mergeWith,
    cloneDeep,
    isNull,
    isNumber,
    isBoolean,
    merge,
    result,
    add,
} = require("lodash");

const fetch = require('node-fetch');




exports.checkPayment = async (admin, payment) => {
    console.log('request Payment status: ', payment.id);
    const key = "l_9BCFC6A061E788054FD89F1AB38A8F8C84C22760E6CFC938:"; //live (test) key
    const basic = "Basic " + Buffer.from(key).toString("base64");


    return await paymentRequest(payment).then(async (res) => {
        try {
            console.log('got status: ' + res.status);
            if (!payment.location || !payment.id) {
                throw new Error('no location or id');
            }
            await admin.database().ref("bars/" + payment.location + "/records/ccv/" + payment.id + "/status").set(res.status);
            return { id: payment.date, status: res.status, result: res };
        } catch (err) {
            console.error(err);
            return { id: payment.date, status: 'error', error: err.message };
        }

    }).catch(err => {
        return { id: payment.date, status: 'error', error: err.message };
    })


    async function paymentRequest(payment) {
        return new Promise((res, rej) => {
            const url = 'https://vpos-test.jforce.be/vpos/api/v1/transaction';
            const options = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': basic,
                    'reference': payment.ccvId  //random idem reference for now
                },
                method: 'GET',

            }

            console.log('sending request to ccv for transaction ' + payment.ccvId + "  " + payment.id);
            fetch(url, options).then(res => res.json()).then(async (json) => {

                var request;
                try {
                    request = json.filter(a => a.reference == payment.ccvId)[0];
                } catch (e) {
                    //request not found?
                    console.log(JSON.stringify(json, null, 2));
                    console.error(e)
                    return rej(e);
                }

                console.log('got request', request.reference, request.result, request.status);

                return res(request);
            }).catch(err => {
                return rej(err);
            });
        })

    }

};

exports.initiatePayment = async (admin, params, payment) => {
    console.log('initiate: ', JSON.stringify(payment, null, 2));
    const key = "l_9BCFC6A061E788054FD89F1AB38A8F8C84C22760E6CFC938:"; //live (test) key
    const basic = "Basic " + Buffer.from(key).toString("base64");


    return await paymentRequest(payment).then(async (res) => {
        try {
            console.log(res.reference);
            if (res.reference) {
                const cleanReference = res.reference.replace('.', '-');

                await admin.database().ref("bars/" + params.bar + "/records/ccv/" + params.ms + "/ccvId").set(res.reference);
                await admin.database().ref("bars/" + params.bar + "/records/ccv/" + params.ms + "/status").set(res.status);
                await admin.database().ref("webhooks/ccv/" + cleanReference).set({ bar: params.bar, date: params.ms, ccvId: res.reference, id: payment.id });
                return { id: params.ms, status: 'success', ccvId: res.id };
            } else {
                return { id: params.ms, status: 'error', error: 'no id' };
            }
        } catch (err) {
            return { id: params.ms, status: 'error', error: err.message };
        }

    }).catch(err => {
        return { id: params.ms, status: 'error', error: err.message };
    })


    async function paymentRequest(payment) {
        return new Promise((res, rej) => {
            const url = 'https://vpos-test.jforce.be/vpos/api/v1/payment'
            const options = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': basic,
                    'Idempotency-Reference': payment.id  //random idem reference for now
                },
                method: 'POST',
                body: JSON.stringify({
                    "currency": "EUR",
                    "amount": Number(payment.amount).toFixed(2), //random amount between 1 and 10
                    "method": "terminal",
                    "language": "nld",
                    "returnUrl": 'https://shop.base.url/sub-url', //is this needed as athing right now?
                    "webhookUrl": 'https://us-central1-staffapp-b0578.cloudfunctions.net/ccvWebhook', //test webhook url
                    "details": {
                        "operatingEnvironment": "ATTENDED",
                        "merchantLanguage": "NLD",
                        "managementSystemId": "GrundmasterNL-ThirdPartyTest",
                        "terminalId": payment.machineId,
                        "accessProtocol": "OPI_NL"
                    },
                    "billingEmail": payment.email
                })
            }

            console.log('sending request to ' + payment.machineId + " for " + payment.amount + " euro");
            fetch(url, options).then(res => res.json()).then(async (json) => {
                return res(json)
            }).catch(err => {
                return rej(err);
            });
        })

    }

};

