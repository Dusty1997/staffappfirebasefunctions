const e = require('express');
const nodemailer = require('nodemailer');
const transporter = nodemailer.createTransport({
    host: 'smtp.hostnet.nl',
    port: 587,
    secure: false,
    auth: {
        user: 'facturen@daga-beheer.nl',
        pass: 'asdasjhkAI(S*&D98asdj!KJ'
    },
    tls: {
        ciphers: 'SSLv3'
    }
});

const transporter2 = nodemailer.createTransport({
    host: 'smtp.hostnet.nl',
    port: 587,
    secure: false,
    auth: {
        user: 'receipts@theendkaraoke.nl',
        pass: 'f*gpzCN7zXCLT#V'
    },
    tls: {
        ciphers: 'SSLv3'
    }
});

const transporter3 = nodemailer.createTransport({
    host: 'smtp.hostnet.nl',
    port: 587,
    secure: false,
    auth: {
        user: 'walkins@theendkaraoke.nl',
        pass: 'aS*(d7A*&SD^ga87s6d0g'
    },
    tls: {
        ciphers: 'SSLv3'
    }
});

const transporter4 = nodemailer.createTransport({
    host: 'smtp.hostnet.nl',
    port: 587,
    secure: false,
    auth: {
        user: 'reports@theendkaraoke.nl',
        pass: 'f*gpzCN7zXCLT#V'
    },
    tls: {
        ciphers: 'SSLv3'
    }
});

exports.sendEmail = async (emailData) => {
    return new Promise((resolve, reject) => {
        //EMAILdATE IS EITHER AN OBJECT OR A STRING
        if (typeof emailData === 'string') {
            emailData = JSON.parse(emailData);
        }
        const { innerHTML, to, subject, text, pdfBase64, filename = 'na', type = 'invoice', filetype = 'pdf' } = emailData;
        var from = '';
        switch (type) {
            case 'invoice':
                from = 'facturen@daga-beheer.nl';
                break;
            case 'receipt':
                from = 'receipts@theendkaraoke.nl';
                break;
            case 'walkin':
                from = 'walkins@theendkaraoke.nl'
                break;
            case 'report':
                from = 'reports@theendkaraoke.nl'
                break;
        }

        console.log('Sending email with options', to, type);
        var mailOptions = {
            from: from,
            to: to,
            subject: subject,

        }
        if (text) {
            mailOptions.text = text
        }

        if (innerHTML) {
            mailOptions.html = innerHTML
        }
        console.log('Sending email with options', mailOptions.from, mailOptions.to);

        if (pdfBase64) {
            mailOptions.attachments = [{
                filename: filename + '.' + filetype,
                content: pdfBase64,
                encoding: 'base64',
            },];
        }

        var trans;

        if (type == 'receipt') {
            trans = transporter2;
        } else if (type == 'invoice') {
            trans = transporter;
        } else if (type == 'walkin') {
            trans = transporter3;
        } else {
            trans = transporter4;
        }

        trans.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log('Error sending email', error);
                return reject(error);
            }
            console.log('Email sent: ' + info.response);
            return resolve('Email sent to : ' + mailOptions.to + ' from ' + mailOptions.from + info.response);
        });
    });

}