
const fetch = require('node-fetch');

const key = "l_9BCFC6A061E788054FD89F1AB38A8F8C84C22760E6CFC938:"; //live (test) key

const basic = "Basic " + btoa(key);  //Key to Base64 Encoded

console.log(Number(process.argv[2]))
const url = 'https://vpos-test.jforce.be/vpos/api/v1/payment'
const options = {
    headers: {
        'Content-Type': 'application/json',
        'Authorization': basic,
        'Idempotency-Reference': 'hello123456' + new Date().getTime()  //random idem reference for now
    },
    method: 'POST',
    body: JSON.stringify({
        "currency": "EUR",
        "amount": Number(process.argv[2]) || Math.min(Math.round(Math.random() * 10, 2), 1), //random amount between 1 and 10
        "method": "terminal",
        "language": "nld",
        "returnUrl": 'https://shop.base.url/sub-url', //is this needed as athing right now?
        "webhookUrl": 'https://us-central1-staffapp-b0578.cloudfunctions.net/ccvWebhook', //test webhook url
        "details": {
            "operatingEnvironment": "ATTENDED",
            "merchantLanguage": "NLD",
            "managementSystemId": "GrundmasterNL-ThirdPartyTest",
            "terminalId": "LEEN0217",
            "accessProtocol": "OPI_NL"
        }

    })
}

console.log('sending request');
fetch(url, options).then(res => res.json()).then(json => console.log(json))