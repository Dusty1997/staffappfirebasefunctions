/* eslint-disable no-trailing-spaces */
/* eslint-disable padded-blocks */
/* eslint-disable no-undef */
/* eslint-disable object-curly-spacing */
/* eslint-disable indent */
/* eslint-disable no-var */
/* eslint-disable max-len */
/* eslint-disable guard-for-in */
/* eslint-disable prefer-const */
/* eslint-disable require-jsdoc */
/* eslint-disable prefer-promise-reject-errors */

const events = require("events");

const http = require("http");
const {
    mergeWith,
    cloneDeep,
    isNull,
    isNumber,
    isBoolean,
    merge,
    result,
    add,
} = require("lodash");


const { getTillInfo, getValuesForDates, getStock, getValuesForBetweenDates } = require("./generic");
var https = require('https');
exports.calculateStaffDrinks = (admin, context) => {
    return new Promise((resolve, reject) => {
        let target = { rate: 1 };

        const hours = {};
        staffNames = {};
        console.log(
            "from:" +
            new Date(context.start).toISOString().split("T")[0] +
            " - to: " +
            new Date(context.end).toISOString().split("T")[0]
        );

        doCalc()
            .then((a) => resolve(a))
            .catch((e) => resolve(e));

        async function doCalc() {
            console.log("check hour review " + context.barname + ".");
            var err = await getHourReviews()
                .then((a) => null)
                .catch((e) => {
                    console.log("no review" + context.barname);
                    return "no review";
                });
            if (err) {
                console.log("cancelling" + context.barname, err);
                return err;
            }

            console.log("get targets " + context.barname + ".");
            err = await getTargets()
                .then((a) => null)
                .catch((e) => "no targets");
            if (err) {
                return err;
            }

            console.log('got targets', this.target)

            console.log("get hours " + context.barname + ".");
            err = await getHoursForMonth(
                new Date(context.start),
                new Date(context.end)
            )
                .then((a) => a)
                .catch((e) => e);
            console.log(
                "get hours done" + context.barname + " " + JSON.stringify(err)
            );

            err = await trySolve()
                .then((a) => a)
                .catch((e) => e);
            console.log("done " + context.barname + ". " + err);
            return JSON.stringify(err);
        }

        function getTargets() {
            return new Promise((res, rej) => {
                admin
                    .database()
                    .ref("admin/bars/" + context.barname + "/drinkCalculation")
                    .once("value", (val) => {
                        target = val.val();
                        if (target) {
                            if (!target.rate) {
                                target.rate = 1;
                            }

                            res();
                        } else {
                            getGlobalTargets(res, rej);
                        }
                    });
            });
        }

        function getGlobalTargets(res, rej) {
            admin
                .database()
                .ref("admin/bars/drinkCalculation")
                .once("value", (val) => {
                    target = val.val();
                    if (target) {
                        if (!target.rate) {
                            target.rate = 1;
                        }
                        targetsDone = true;
                        return res();
                    } else {
                        target = { rate: 1 };
                        targetsDone = true;
                        return res();

                        // reject("no target calculations");
                    }
                });
        }

        function getHoursForMonth(start, end) {
            return new Promise((res, rej) => {
                getHoursForMonthCall(res, rej, start, end);
            });
        }

        function getHoursForMonthCall(res, rej, start, end) {
            if (start > end) {
                hoursDone = true;
                return res();
            } else {
                admin
                    .database()
                    .ref(
                        "bars/" +
                        context.barname +
                        "/hours/" +
                        start.getFullYear() +
                        "/" +
                        start.getMonth() +
                        "/" +
                        start.getDate()
                    )
                    .once("value", (val) => {
                        if (val) {
                            val.forEach((user) => {
                                const u = user.val();
                                // console.log('adding user hours', u.key, u.total);
                                if (u) {
                                    if (hours[u.key]) {
                                        hours[u.key] += u.total;
                                    } else {
                                        hours[u.key] = u.total;
                                    }
                                    staffNames[u.key] = u.name;
                                }
                            });
                        }

                        start.setDate(start.getDate() + 1);

                        return getHoursForMonthCall(res, rej, start, end);
                    });
            }
        }

        async function trySolve() {
            for (var h in hours) {
                if (hours[h] > Math.min(target.targetHours, 5)) {
                    console.log(h + ' has' + hours[h] + ' hours and target is ' + target.targetHours + ' so they get an extra' + target.targetRate)
                    hours[h] = Number(hours[h]) * Number(target.rate) + target.targetRate;
                } else {
                    hours[h] = Number(hours[h]) * Number(target.rate);
                }

            }

            // eslint-disable-next-line no-redeclare
            return writeBalance(admin, hours, context);
        }

        function getHourReviews() {
            return new Promise((res, rej) => {
                admin
                    .database()
                    .ref("bars/" + context.barname + "/records/hourReviews")
                    .orderByChild("timeOfReview")
                    .limitToLast(5)
                    .once("value", (val) => {
                        const date = new Date(); //tuesday 7am, check if review is from sunday morning morning till next saturday night

                        var found = false;
                        val.forEach((child) => {
                            const data = child.val();

                            if (
                                sameWeek(new Date(data.timeOfReview), new Date(context.end))
                            ) {
                                console.log(
                                    "reviewed",
                                    new Date(data.timeOfReview).toDateString(),
                                    context.barname
                                );
                                found = true;
                                return res();
                            }
                        });

                        console.log("data", found, context.force);
                        if (found || context.force) {
                            return res();
                        } else {
                            return rej();
                        }
                    });
            });
        }

        function sameWeek(date2, today) {
            //tuesday 7am,
            const firstDay = new Date(
                today.getFullYear(),
                today.getMonth(),
                today.getDate() - 2,
                0,
                0,
                0,
                0
            );
            const lastDay = new Date(
                today.getFullYear(),
                today.getMonth(),
                today.getDate() + 4,
                23,
                59,
                99,
                999
            );

            if (
                firstDay.getTime() <= date2.getTime() &&
                lastDay.getTime() >= date2.getTime()
            ) {
                return true;
            }
            return false;
        }
        // update users with their hours

        async function writeBalance(admin, hours, context) {
            console.log(context.barname + " has rate:" + target.rate);
            if (context.fake) {
                return Object.entries(hours);
            }
            for (const h in hours) {
                try {
                    await admin
                        .database()
                        .ref("staff/" + h + "/balance")
                        .once("value", (snapshot) => {
                            if (Number(snapshot.val())) {
                                var number =
                                    Math.round(
                                        Number(Number(snapshot.val()) + Number(hours[h])) * 100
                                    ) / 100;
                                admin
                                    .database()
                                    .ref("staff/" + h + "/balance")
                                    .set(number);
                            } else {
                                admin
                                    .database()
                                    .ref("staff/" + h + "/balance")
                                    .set(Number(hours[h]));
                            }
                        });

                    try {
                        const start = new Date(context.start).toLocaleDateString('en-UK');
                        const end = new Date(context.end).toLocaleDateString('en-UK');



                        var weekString = start + " - " + end;
                        await admin
                            .database()
                            .ref("stafforders/" + h + "/" + new Date().getTime())
                            .set({
                                bar: context.barname,
                                total: hours[h],
                                type: "credit",
                                reason: "Weekly Gift Function: " + weekString,
                            });
                    } catch (e) {
                        console.log(e);
                    }
                } catch (e) {
                    console.log(e);
                }
            }

            return Object.entries(hours);
        }
        // eslint-disable-next-line eol-last
    });

    // eslint-disable-next-line eol-last
};

exports.getDiscounts = (admin, bar, from, till, save = true) => {
    return new Promise((res, rej) => {
        getDiscounts()
            .then((then) => {
                console.log("bar returning", bar);
                return res(then);
            })
            .catch((e) => {
                console.error(e);

                return rej(e);
            });
    });

    async function getDiscounts() {
        try {
            console.log("GET DISCOUNTS" + "- Getting: " + bar);
            const info = await getInfo(bar)
                .then((a) => a)
                .catch((e) => {
                    console.log("cant get info", bar, e);
                    return null;
                });

            if (!info) {
                return;
            }
            //   console.log("GET DISCOUNTS" + "- GotInfo:");

            const data = await getData(from, till, info)
                .then((a) => a)
                .catch((e) => {
                    console.log(e);
                    return "";
                });
            // console.log("parsing data", typeof data, data);
            const fin = await findDiscounts(JSON.parse(data))
                .then((a) => a)
                .catch((e) => {
                    console.log("error finding discount", e);
                    return [];
                });
            //console.log(fin);

            if (!save) {
                return fin;
            }

            const saved = await saveDiscounts(fin)
                .then((a) => {
                    console.log(bar, "saved all");
                    return fin;
                })
                .catch((e) => {
                    console.log("error savig discount", e);
                    return [];
                });

            //   console.log("returning ", saved.length);
            return saved;
        } catch (e) {
            console.error(e);
            console.log("get discounts error", bar, e);
            return e;
        }
    }

    async function getInfo(barname) {
        return new Promise((res, rej) => {
            admin
                .database()
                .ref("bars/" + barname + "/tillInformation")
                .once("value", (snap) => {
                    // console.log("tillinfo for: " + barname + ", info: ", snap.val());
                    if (snap && snap.val()) {
                        return res(snap.val());
                    }
                    return rej({});
                });
        });
    }

    async function getData(startDate, endDate, tillInfo) {
        //console.log(startDate, endDate)

        startDate = new Date(startDate);
        endDate = new Date(endDate);

        const till = new Date(
            endDate.getFullYear(),
            endDate.getMonth(),
            endDate.getDate(),
            6,
            59,
            59,
            999
        );
        const from = new Date(
            startDate.getFullYear(),
            startDate.getMonth(),
            startDate.getDate(),
            7,
            30,
            0,
            0
        );
        const auth = Buffer.from(
            tillInfo.username + ":" + tillInfo.password
        ).toString("base64");
        // console.log('auth', auth)

        const query = new URLSearchParams();
        query.set("from", from.toISOString().split("Z")[0]);
        query.set("till", till.toISOString().split("Z")[0]);

        //console.log(query);
        const options = {
            host: tillInfo.path.split(":")[1].split("//")[1],
            port: tillInfo.path.split(":")[2].split("/")[0],
            path: "/api/v1" +
                "/" +
                tillInfo.database +
                "/turnover-transaction?" +
                query.toString(),
            method: "GET",

            headers: {
                "content-type": "application/json",
                Authorization: "Basic " + auth,
                AppToken: tillInfo.token,
            },
        };
        // console.log(options)

        return new Promise((res3, rej) => {
            const req = http.request(options, (res) => {
                // console.log(res.statusMessage)
                // console.log(`statusCode: ${res.statusCode}`);

                var str = "";
                res.on("data", (d) => {
                    str += d;
                });

                res.on("end", function () {
                    return res3(str);
                });
            });

            req.on("error", (error) => {
                console.log("error gettung data");
                console.error(error);
                return rej("");
            });

            req.end();
        });
    }

    async function findDiscounts(reports = []) {
        try {
            if (!reports.length) {
                return [];
            }
            const filterMap = (checker, mapper, list) => [...list].reduce(
                (acc, current) =>
                    checker(current) ? acc.push(mapper(current)) && acc : acc, []
            );

            console.log("findDiscounts");
            reports = filterMap(
                (report) => {
                    if (
                        Array.from(report.orders).filter((order) => {
                            if (
                                [...order.items].filter((item) => item.discountReason).length
                            ) {
                                return true;
                            }
                            return false;
                        }).length
                    ) {
                        return true;
                    }
                    return false;
                },
                (report) => {
                    var reason = "";
                    var drinks = [];
                    var quantity = 0;
                    const amount = Array.from(report.orders)
                        .map((a) => {
                            return [...a.items]
                                .map((b) => {
                                    if (b.discountReason) {
                                        quantity += b.quantity;
                                        reason = b.discountReason.name;
                                        drinks.push(b.articleId);
                                        return Math.max(b.discount, 0);
                                    }
                                    return 0;
                                })
                                .reduce((val, val2) => val + val2, 0);
                        })
                        .reduce((val, val2) => val + val2, 0);

                    var data = {
                        id: String(report.id),
                        dateTime: new Date(report.closeDateTime).getTime(),
                        totalAmount: Number(amount.toFixed(2)),
                        reason: reason,
                        drinks: drinks,
                        quantity: quantity,
                    };
                    return data;
                },
                reports
            );
        } catch (e) {
            console.log("failed to get", e);
        }

        return reports;
    }

    async function saveDiscounts(discounts = []) {
        //console.log('saving discounts', discounts.length);
        const yesterday = new Date(from);
        var year = yesterday.getFullYear();
        var month = yesterday.getMonth();
        var day = yesterday.getDate();

        var promises = [];
        for (var i = 0; i < discounts.length; i++) {
            promises.push(
                admin
                    .database()
                    .ref(
                        "bars/" +
                        bar +
                        "/records/discounts/" +
                        year +
                        "/" +
                        month +
                        "/" +
                        day +
                        "/" +
                        discounts[i].dateTime
                    )
                    .set(discounts[i])
            );
        }

        return Promise.all(promises)
            .then((a) => {
                console.log("saves resolved", bar);
                return discounts;
            })
            .catch((e) => {
                console.log(e);
                return e;
            });
    }
};

exports.getDetailedTurnover = async (admin, bar, from, till, info) => {


    try {


        if (!info) {
            throw new Error('no info');
        }


        const data = await getData(from, till, info)
            .then((a) => a)
            .catch((e) => {
                console.log(e);
                return "";
            });

        console.log('parsring and retunrnig date: ' + new Date(from))
        return data;
    } catch (e) {
        console.error(e);
        console.log("get discounts error", bar, e);
        return e;
    }


    async function getData(startDate, endDate, tillInfo) {
        //console.log(startDate, endDate)

        startDate = new Date(startDate);
        endDate = new Date(endDate);

        const till = new Date(
            endDate.getFullYear(),
            endDate.getMonth(),
            endDate.getDate(),
            6,
            59,
            59,
            999
        );
        const from = new Date(
            startDate.getFullYear(),
            startDate.getMonth(),
            startDate.getDate(),
            7,
            30,
            0,
            0
        );
        const auth = Buffer.from(
            tillInfo.username + ":" + tillInfo.password
        ).toString("base64");
        // console.log('auth', auth)

        const query = new URLSearchParams();
        query.set("from", from.toISOString().split("Z")[0]);
        query.set("till", till.toISOString().split("Z")[0]);

        //console.log(query);
        const options = {
            host: tillInfo.path.split(":")[1].split("//")[1],
            port: tillInfo.path.split(":")[2].split("/")[0],
            path: "/api/v1" +
                "/" +
                tillInfo.database +
                "/turnover-bill?" +
                query.toString(),
            method: "GET",

            headers: {
                "content-type": "application/json",
                Authorization: "Basic " + auth,
                AppToken: tillInfo.token,
            },
        };
        console.log('http requesting', from.getMonth() + "/" + from.getDate(), till.getMonth() + "/" + till.getDate())

        return await (new Promise((res3, rej) => {
            http.get(options, (res) => {
                console.log(res.statusMessage)
                console.log(`statusCode: ${res.statusCode}`);

                var str = "";
                res.on("data", (d) => {
                    str += d;
                });

                res.on("end", function () {
                    console.log('ended')

                    return res3(str);
                });

                res.on("error", (error) => {
                    console.log("error gettung data");
                    console.error(error);
                    return rej({ error: error });
                });
            });






        })).then(a => a).catch(e => e);
    }

};

const inoutcash = async (admin, info, bar, date) => {
    if (!info) {
        return { status: "fail", reason: "no info" };
    }

    var end = new Date(date);
    end = new Date(end.getFullYear(), end.getMonth(), end.getDate(), 7, 30, 0, 0);
    var start = new Date(end.getFullYear(), end.getMonth(), end.getDate() - 1, 7, 29, 59, 999);


    const data = JSON.parse(await getData(start, end, info).then(a => a).catch(e => {
        console.error(e);
        return null;
    }));

    if (!data) {
        return { status: "fail", reason: "no data" };
    }

    const save = await saveData(start, data)
        .then((a) => null)
        .catch((e) => e);
    if (save) {
        return { status: "fail", reason: "couldnt save" + JSON.stringify(save) };
    }

    return { data };

    async function getData(startDate, endDate, tillInfo) {


        startDate = new Date(startDate);
        endDate = new Date(endDate);

        const till = new Date(
            endDate.getFullYear(),
            endDate.getMonth(),
            endDate.getDate(),
            6,
            59,
            59,
            999
        );
        const from = new Date(
            startDate.getFullYear(),
            startDate.getMonth(),
            startDate.getDate(),
            7,
            30,
            0,
            0
        );
        const auth = Buffer.from(
            tillInfo.username + ":" + tillInfo.password
        ).toString("base64");


        const query = new URLSearchParams();
        query.set("from", from.toISOString().split("Z")[0]);
        query.set("till", till.toISOString().split("Z")[0]);

        const options = {
            host: tillInfo.path.split(":")[1].split("//")[1],
            port: tillInfo.path.split(":")[2].split("/")[0],
            path: "/api/v1" +
                "/" +
                tillInfo.database +
                "/in-out-cash-item?" +
                query.toString(),
            method: "GET",

            headers: {
                "content-type": "application/json",
                Authorization: "Basic " + auth,
                AppToken: tillInfo.token,
            },
        };

        return new Promise((res3, rej) => {
            const req = http.request(options, (res) => {

                var str = "";
                res.on("data", (d) => {
                    str += d;
                });

                res.on("end", function () {
                    return res3(str);
                });
            });

            req.on("error", (error) => {
                console.log("error gettung data");
                console.error(error);
                return rej("");
            });

            req.end();
        });
    }

    async function saveData(start, discounts) {
        console.log(
            `saving ${bar} with items: ${data.length} ` + JSON.stringify(discounts)
        );
        const yesterday = new Date(start);
        var year = yesterday.getFullYear();
        var month = yesterday.getMonth();
        var day = yesterday.getDate();

        var promises = [];

        try {
            var total = 0;
            for (var i = 0; i < discounts.length; i++) {
                total += Number(discounts[i].amount);
            }
        } catch (e) {
            console.error("saving losten error", e);
        }

        for (var i = 0; i < discounts.length; i++) {
            discounts[i].dateTime = new Date(discounts[i].timestamp).getTime();
            promises.push(
                admin
                    .database()
                    .ref(
                        "bars/" +
                        bar +
                        "/records/cashinout/" +
                        year +
                        "/" +
                        month +
                        "/" +
                        discounts[i].dateTime
                    )
                    .set(discounts[i])
            );
        }


        return Promise.all(promises)
            .then((a) => {
                console.log("saves resolved", bar);
                return discounts;
            })
            .catch((e) => {
                console.log(e);
                return e;
            });
    }
};
exports.getInOutCashItem = inoutcash;

const inoutdb = async (admin, bar, date) => {
    return new Promise((res, rej) => {
        admin.database().ref("bars/" +
            bar +
            "/records/cashinout/" + date.getFullYear() + "/" + date.getMonth()).once("value", (snap) => {
                try {
                    const list = [];
                    snap.forEach((data) => {
                        list.push(data.val());
                    });
                    return res(list);
                } catch (e) {
                    return rej(e);
                }
            });
    })

}

const vasteLasten = async (admin, info, bar, date) => {
    const bonnen = await getBonnen()
        .then((a) => a)
        .catch((e) => {
            console.error("get bonnen " + bar, JSON.stringify(e));
            return [];
        });

    const vaste = await getVaste()
        .then((a) => a)
        .catch((e) => {
            console.error("get vaste " + bar, JSON.stringify(e));
            return [];
        });

    var vTotal = 0;
    for (var i = 0; i < vaste.length; i++) {
        vTotal += Number(vaste[i].price);
    }

    var bTotal = 0;
    for (var i = 0; i < bonnen.length; i++) {
        bTotal += Number(bonnen[i].price);
    }



    return {
        bonnen: bonnen,
        vaste: vaste,
    };

    async function save(vaste, bonnen) {
        await admin
            .database()
            .ref(
                "bars/" +
                bar +
                "/records/kostenoverview/" +
                date.getFullYear() +
                "/" +
                date.getMonth() +
                "/bonnen"
            )
            .set(bonnen);
        await admin
            .database()
            .ref(
                "bars/" +
                bar +
                "/records/kostenoverview/" +
                date.getFullYear() +
                "/" +
                date.getMonth() +
                "/vaste"
            )
            .set(vaste);
    }

    //save to
    // bars/${bar}/records/kostenoverview/
    //vastelasten
    //bonnen

    function getBonnen() {
        return new Promise((res, rej) => {

            admin

                .database()
                .ref(
                    "bars/" +
                    bar +
                    "/records/costs/" +
                    date.getFullYear() +
                    "/" +
                    date.getMonth()
                )
                .once("value", (snap) => {
                    try {
                        const list = [];
                        snap.forEach((data) => {
                            list.push(data.val());
                        });
                        return res(list);
                    } catch (e) {
                        return rej(e);
                    }
                });
        });
    }

    function getVaste() {
        return new Promise((res, rej) => {
            admin
                .database()
                .ref("bars/" + bar + "/expenses")
                .once("value", (snap) => {
                    try {
                        const list = [];
                        snap.forEach((data) => {
                            const val = data.val();

                            if (
                                date.getTime() >= val.startDate &&
                                (date.getTime() <= val.endDate || !val.endDate)
                            ) {
                                list.push(data.val());
                            }
                        });
                        return res(list);
                    } catch (e) {
                        return rej(e);
                    }
                });
        });
    }
};

exports.monthlyBuild = async (admin, barOverride, date) => {
    var now = new Date();
    if (date) {
        now = new Date(date);
        now = new Date(now.getFullYear(), now.getMonth(), 1, 7, 0, 0, 1);
    } else {
        now = new Date(now.getFullYear(), now.getMonth() - 1, 1, 7, 0, 0, 1);
    }

    date = new Date(now);

    console.log("doing monthly build:" + now.toISOString());

    var bars = await getBars()
        .then((a) => a)
        .catch((e) => {
            console.log(e);
            return [];
        });

    if (barOverride) {
        console.log("overring bar", barOverride);
        bars = [barOverride];
    }

    var results = [];
    for (var i = 0; i < bars.length; i++) {
        const bar = bars[i];
        if (bar == "Pertempto" || bar == "Daga Beheer") continue;
        try {
            const tillInfo = await getInfo(bar)
                .then((a) => a)
                .catch((e) => {
                    console.log(e);
                    return {};
                });

            if (!tillInfo) {
                results.push({ bar: bar, status: "fail", reason: "no till info" });
                return;
            }


            const inout = await inoutdb(admin, bar, new Date(date)).then(a => a).catch(e => {
                console.error(e);
                return [];
            })



            const recurringCosts = await vasteLasten(admin, tillInfo, bar, new Date(date))
                .then((a) => a)
                .catch((e) => {
                    console.error(JSON.stringify(e));
                    return null;
                });

            if (!recurringCosts) {
                results.push({
                    bar: bar,
                    status: "fail",
                    reason: "check logs",
                    task: "recurringCosts",
                });
            }

            var orders = [];

            const data = await getData(bar, new Date(date), recurringCosts, orders)
                .then((a) => a)
                .catch((e) => {
                    console.error(e);

                    return { error: e };
                });
            data.inout = inout;

            await saveMonth(bar, new Date(date), data);

            results.push({
                bar: bar,
                status: "success",
                data: data,
            });
        } catch (e) {
            console.error(e);
            results.push({ bar: bars[i], status: "fail", reason: e });
        }
    }

    return JSON.stringify(results);

    async function saveMonth(bar, date, data) {
        console.log('saving: ' + "bars/" +
            bar +
            "/records/kostenoverview/" +
            date.getFullYear() +
            "/" +
            date.getMonth())
        await admin
            .database()
            .ref(
                "bars/" +
                bar +
                "/records/kostenoverview/" +
                date.getFullYear() +
                "/" +
                date.getMonth()
            )
            .set(data);
    }

    function getBars() {
        return new Promise((res, rej) => {
            try {
                const bars = [];
                admin
                    .database()
                    .ref("barnames")
                    .once("value", (snap) => {
                        snap.forEach((bar) => {
                            const data = bar.val();
                            bars.push(data);
                        });

                        return res(bars);
                    });
            } catch (e) {
                return rej(e);
            }
        });
    }

    async function getInfo(barname) {
        return new Promise((res, rej) => {
            admin
                .database()
                .ref("bars/" + barname + "/tillInformation")
                .once("value", (snap) => {

                    if (snap && snap.val()) {
                        return res(snap.val());
                    }
                    return rej({});
                });
        });
    }

    async function getData(bar, date, recurring, orders) {
        //last day of month
        const endDate = new Date(
            date.getFullYear(),
            date.getMonth() + 1,
            1,
            6,
            59,
            59,
            999
        );
        const month = new Date(date).toISOString().split("T")[0];
        var list = [];

        const skeleton = {
            cash: 0,
            pin: 0,
            total: 0,
            internet: 0,
            teller: 0,
            flesjes: 0,
            checks: {
                cash: true,
                pin: true,
                total: true,
                internet: true,
                teller: true,
                flesjes: true,
                bonnen: true
            },
            differences: {
                cash: 0,
                pin: 0,
                total: 0,
                internet: 0,
                teller: 0,
                flesjes: 0,
                bonnen: 0
            },
        };
        while (date < endDate) {

            var worked = await getHours(bar, date)
                .then((a) => a)
                .catch((e) => {
                    console.error("worked " + e.toString());
                    return [];
                });
            var verbruik = await getVerbruik(bar, date)
                .then((a) => a)
                .catch((e) => {
                    console.error("verbruik " + e.toString());
                    return [];
                });
            var omzet = await getOmzet(bar, date)
                .then((a) => a)
                .catch((e) => {
                    console.error("omzet " + e.toString());
                    return {};
                });

            const tillDate = date.toISOString().split("T")[0];

            list.push({
                key: tillDate,
                worked: worked,
                verbruik: verbruik,
                omzet: omzet,
            });

            date.setDate(date.getDate() + 1);
        }

        var reduced = list.reduce(
            (prev, curr) => {
                //prev worked should be a list people who worked and how long, with a sum of the difference for that day
                try {
                    var difference = 0;
                    curr.worked.forEach((wo) => {
                        if (!prev.worked[wo.key]) {
                            prev.worked[wo.key] = wo;
                            prev.worked[wo.key].difference = difference;
                            prev.worked[wo.key].cost = wo.total * (wo.price || 0);
                        } else {
                            prev.worked[wo.key].total += wo.total;
                            prev.worked[wo.key].difference += difference;
                            prev.worked[wo.key].cost += wo.total * (wo.price || 0);
                        }
                    });

                    //verbruik should have three sublists which each has all their respective for that month

                    curr.verbruik.forEach((disc) => {
                        if (disc.reason.includes("Medewerker")) {
                            prev.verbruik.verbruik.push(disc);
                            return;
                        }

                        if (
                            disc.reason.includes("Wisselen") ||
                            disc.reason.includes("Einde")
                        ) {
                            prev.verbruik.wisselen.push(disc);
                            return;
                        }

                        prev.verbruik.spill.push(disc);
                    });

                    const skel = cloneDeep(skeleton);
                    var fin;
                    if (!curr || !curr.omzet || !curr.omzet.final) {
                        fin = skel;
                    } else {
                        fin = merge(skel, curr.omzet.final);
                    }



                    fin.checks.total = fin.checks.cash && fin.checks.pin && fin.checks.internet && fin.checks.flesjes && (String(fin.checks.bonnen) === 'false' ? false : true) && fin.checks.teller;

                    prev.omzet = mergeWith(prev.omzet, fin, (obj, val) => {

                        if (String(val) === 'true' || String(val) === 'false') {
                            if (!val) {
                                console.log(JSON.stringify(fin) + 'has false', obj, val);
                            }
                            return obj && val;
                            //(String(obj) === 'false' ? false : true);
                        }
                        const result = add(obj, val)


                        // ignore NaN values and return undefined instead
                        if (isNaN(result)) {
                            return;
                        }

                        //only combine values that can be combined
                        return result;
                    });

                    console.log('omzet: differences: total: ' + (prev.omzet.differences.cash + prev.omzet.differences.pin +
                        prev.omzet.differences.internet + prev.omzet.differences.teller +
                        prev.omzet.differences.flesjes + prev.omzet.differences.bonnen));

                    console.log('omzet: differences: breakdown: ' + prev.omzet.differences.cash + ' ' + prev.omzet.differences.pin +
                        ' ' + prev.omzet.differences.internet + ' ' + prev.omzet.differences.teller +
                        ' ' + prev.omzet.differences.flesjes + ' ' + prev.omzet.differences.bonnen);

                    prev.omzet.differences.total = prev.omzet.differences.cash + prev.omzet.differences.pin +
                        prev.omzet.differences.internet + prev.omzet.differences.teller +
                        prev.omzet.differences.flesjes + prev.omzet.differences.bonnen



                } catch (e) {
                    console.error(curr.key, e);
                    console.log(e.trace);
                    return prev;
                }

                //omzet is totals

                //kosten sub lists are the list of oneoffs, list of inout, and list of orders
                return prev;
            }, {
            worked: {},
            verbruik: { verbruik: [], spill: [], wisselen: [] },
            omzet: cloneDeep(skeleton),
            kosten: { recurring: [], oneoff: [], orders: [] },
        }
        );
        reduced.kosten.recurring = recurring.vaste;
        reduced.kosten.oneoff = recurring.bonnen;

        reduced.kosten.orders = orders;
        reduced.verbruik.verbruik = verbruikReducer(
            reduced.verbruik.verbruik,
            "verbruik"
        );
        reduced.verbruik.spill = verbruikReducer(
            reduced.verbruik.spill,
            "verbruik"
        );
        reduced.verbruik.wisselen = verbruikReducer(
            reduced.verbruik.wisselen,
            "verbruik"
        );
        reduced.key = month;

        return reduced;
    }

    function verbruikReducer(verbruik, key) {
        return verbruik.reduce(
            (p, c) => {
                p.total += c.totalAmount;
                p.quantity += c.drinks.length;
                if (!p.reasons[c.reason]) {
                    p.reasons[c.reason] = {
                        reason: c.reason,
                        quantity: c.drinks.length,
                        total: c.totalAmount,
                    };
                } else {
                    p.reasons[c.reason].quantity += c.drinks.length;
                    p.reasons[c.reason].total += c.totalAmount;
                }
                return p;
            }, { total: 0, quantity: 0, key: key, reasons: {} }
        );
    }

    async function getHours(bar, date) {
        return new Promise((res, rej) => {
            admin
                .database()
                .ref(
                    "bars/" +
                    bar +
                    "/hours/" +
                    date.getFullYear() +
                    "/" +
                    date.getMonth() +
                    "/" +
                    date.getDate()
                )
                .once("value", (val) => {
                    var list = [];
                    if (val) {
                        val.forEach((user) => {
                            const u = user.val();
                            // console.log('adding user hours', u.key, u.total);
                            list.push({ key: u.key, total: u.total, name: u.name });
                        });
                    }

                    res(list);
                });
        });
    }

    async function getVerbruik(bar, date) {
        /* return await this.staffAppBase.list('bars/' + bar + '/records/discounts/' + date.getFullYear() + '/' + (date.getMonth()) + '/' + date.getDate()).valueChanges().pipe(take(1)).toPromise();*/
        return new Promise((res, rej) => {
            admin
                .database()
                .ref(
                    "bars/" +
                    bar +
                    "/records/discounts/" +
                    date.getFullYear() +
                    "/" +
                    date.getMonth() +
                    "/" +
                    date.getDate()
                )
                .once("value", (val) => {
                    var list = [];
                    if (val) {
                        val.forEach((user) => {
                            const u = user.val();
                            // console.log('adding user hours', u.key, u.total);
                            list.push(u);
                        });
                    }

                    res(list);
                });
        });
    }

    async function getOmzet(bar, date) {

        /* return await this.staffAppBase.list('bars/' + bar + '/records/discounts/' + date.getFullYear() + '/' + (date.getMonth()) + '/' + date.getDate()).valueChanges().pipe(take(1)).toPromise();*/
        return new Promise((res, rej) => {
            admin
                .database()
                .ref(
                    "bars/" +
                    bar +
                    "/records/till/" +
                    date.getFullYear() +
                    "-" +
                    date.getMonth() +
                    "-" +
                    date.getDate()
                )
                .once("value", (val) => {
                    const data = val.val();
                    //console.log('got omzet: ' + date.getFullYear() + '/' + (date.getMonth()) + '/' + date.getDate() + ' ' + JSON.stringify(data, null, 2));
                    return res(data);
                });
        });
    }

    /*
      this.staffAppBase.object('bars/' + bar + '/records/till/' + date.getFullYear() + '-' + (date.getMonth()) + '-' + date.getDate()).valueChanges().pipe(take(1)).toPromise();
      */
};

exports.getInkoop = async (admin, bar, start) => {
    console.log('getting Inkoop for: ' + bar);
    var results = [];

    start = new Date(start);

    if (!(start instanceof Date && !isNaN(start.valueOf()))) {
        results.push("invalid dates or range: " + start);
        throw new Error({ results: results, status: 'failed' });
    }
    results.push("Getting: " + bar + " for date: " + start.toDateString())

    const info = await getTillInfo(admin, bar)
        .then((a) => a)
        .catch((e) => {
            return null;
        });

    if (!info ||
        !(
            info.cashReference &&
            info.pinReference &&
            info.internetReference &&
            info.url &&
            info.password &&
            info.username &&
            info.token
        )
    ) {
        console.log("no till info: " + bar + " - " + JSON.stringify(info))
        results.push("no till info: " + bar + " - " + JSON.stringify(info));
        throw new Error(JSON.stringify(results));
    }


    var inout = await this.getInOutCashItem(admin, info, bar, start).then(a => a).catch(e => {
        console.error(e);
    })
    results.push(inout);


    var orders = await getValuesForDates(bar, info, start).then(a => a).catch(e => {
        console.error('Error getting ordeers: ', e);
        return null;
    });


    if (!orders) {
        results.push('no orders');
    }

    var stock = await getStock(admin, bar)
        .then((a) => a)
        .catch((e) => {
            return null;
        })

    var drinks = {};
    try {
        Object.values(stock).forEach(type => {
            Object.values(type).forEach(drink => {
                drinks[drink.serverId] = drink;
            })
        })
    } catch (e) {
        console.error('error getting stock: ' + bar, e);
        results.push('error getting stock: ' + bar);
    }


    var total = 0;
    var vatPaid = 0;
    var shots = {};
    var beers = {};
    if (!orders.vats) {
        orders.vats = [];
        console.error('no vats found');
        results.push('no vats found');
    }
    orders.vats.forEach(order => {
        var dri = drinks[order.drink];
        if (dri) {
            if (dri.crateSize <= 1 && !isNaN(dri.singlePrice)) {
                total += order.quantity * (dri.singlePrice || 0);
            } else if (dri.pricePerUnit > 0) {
                total += order.quantity * (dri.pricePerUnit / dri.crateSize);
            } else {
                results.push(order.drink + " pricePerUnit or singlePrice Missing")
            }



            if (dri.type = '')
                vatPaid += order.vatPaid;

        } else {
            results.push({ error: 'Error finding dri ' })
            console.error(order.drink + " not found: " + bar);
        }

    })


    try {

        var names = ['heineken 0.0%', 'apple bandit', 'desperados']
        orders.actualPaidOrders.forEach(async (order) => {
            order.orders.forEach(async (o) => {

                o.items.forEach(async (item) => {

                    var dri = drinks[item.articleId];
                    if (dri && dri.tillCategory && dri.tillCategory.includes('Shotjes') && item.price != 0) {
                        shots[dri.serverId] = (shots[dri.serverId] || 0) + item.quantity;
                    } else if (dri && names.includes(dri.name.toLowerCase())) {
                        // console.log('found beer: ' + dri.name + ' * ' + item.quantity + ' - ' + JSON.stringify(item, null, 2))
                        beers[dri.serverId] = (beers[dri.serverId] || 0) + item.quantity;
                    } else if (!dri) {
                        console.log('coudnt find drink: ' + item.articleId)
                    }
                })


            })

        })
    } catch (e) {
        console.error(e);
        results.push({ error: 'Error getting paid order shots ' });
    }
    results.push(beers);
    results.push(shots);
    orders.data.inkoop = total;
    orders.data.vat = vatPaid;
    orders.data.shots = shots;
    orders.data.beers = beers;


    if (start.getMonth() == 10 && start.getFullYear() == 2024) {
        try {
            console.log('saving competition')
            await saveCompetition(bar, orders.data.shots, "competitionNov24");
        } catch (e) {
            console.error('failed to save competition', e);
        }
    }


    results.push(orders.data);
    await saveValues(orders.data);
    return results;


    async function saveValues(data) {
        const yesterday = new Date(data.yesterday);
        const barname = data.barname;

        var year = yesterday.getFullYear();
        var month = yesterday.getMonth();
        var day = yesterday.getDate();

        const ref = admin.database().ref("bars/" + barname + "/records/till/" + year + "-" + month + "-" + day);

        try {

            await ref.child("internet").set(data.extra);
        } catch (e) {
            console.log('failed to save: internet ');
        }

        try {
            await ref.child("expectedPin").set(data.pin);
        } catch (e) {
            console.log('failed to save: expected Pin');
        }

        try {
            await ref.child("expectedCash").set(data.cash);
        } catch (e) {
            console.log('failed to save: expected Cash');
        }

        try {
            await ref.child("staffDrinks").set(data.staffDrinks);
        } catch (e) {
            console.log('failed to save: staff drinks');
        }

        try {
            await ref.child("other").set(data.other);
        } catch (e) {
            console.log('failed to save: other ');
        }

        try {
            await ref.child('inkoop').set(data.inkoop);
        } catch (e) {
            console.log('failed to save: inkoop');
        }

        try {
            await ref.child('vat').set(data.vat);
        } catch (e) {
            console.log('failed to save: vat');
        }

        try {
            await ref.child('quantities').set(data.quantities);

        } catch (e) {
            console.log('failed to save: quantities');
        }

        try {
            await ref.child('shots').set(data.shots);
        } catch (e) {
            console.log('failed to save: shots');
        }

        try {
            await ref.child('beers').set(data.beers);
        } catch (e) {
            console.log('failed to save: shots');
        }

        try {
            await ref.child('tables').set(data.tables);
        } catch (e) {

            console.log('failed to save: tables', e);
        }






    }

    async function saveCompetition(bar, shots, competition) {
        if (!bar || !competition) {
            return;
        }
        const ref = admin.database().ref("bars/" + bar + "/records/competitions/" + competition);
        //get the current results
        var current = await ref.once("value").then(a => a.val()).catch(e => {
            console.error('failed to get current competition results', e);
            return null;
        });

        if (!current) {
            console.error('no current competition results');
            return;
        }

        console.log('current competition: ', JSON.stringify(shots, null, 2));
        //configure the new results
        var day = start.getDate() - 1;
        console.log('day: ' + day);
        current.days[day].shotTypes = shots;
        current.days[day].shots = Object.values(shots).reduce((a, b) => a + b, 0);

        //recount all the month
        var euros = 0;
        var omzetPercentage = 0;
        var score = 0;
        var shotPercentage = 0;
        var total = 0;
        var yesterday = current.days[day].shots; //last day
        for (var i = 0; i < current.days.length; i++) {
            if (current.days[i].shotTypes) {
                Object.entries(current.days[i].shotTypes).forEach(([key, value]) => {
                    total += value;
                    score += value;
                    euros += value * drinks[key].price;
                });
            }
        }

        current.total = total;
        current.euros = euros;
        current.score = score;
        current.omzetPercentage = omzetPercentage;
        current.shotPercentage = shotPercentage;
        current.yesterday = yesterday;
        console.log('saving competition: ', total, current, total, JSON.stringify(current, null, 2));
        //save the new results
        await ref.set(current).then(a => a).catch(e => {
            console.error('failed to save competition results', e);
        });
    };
};
exports.getShots = async (admin, bar, start) => {
    console.log('getting Inkoop for: ' + bar);
    var results = [];

    start = new Date(start);


    if (!(start instanceof Date && !isNaN(start.valueOf()))) {
        results.push("invalid dates or range: " + start);
        throw new Error({ results: results, status: 'failed' });
    }

    if (start.getHours() < 7) {
        start.setDate(start.getDate() - 1);
    }

    start.setHours(16);
    start.setMinutes(0);
    start.setSeconds(0);
    results.push("Getting: " + bar + " for date: " + start.toDateString())

    const info = await getTillInfo(admin, bar)
        .then((a) => a)
        .catch((e) => {
            return null;
        });

    if (!info ||
        !(
            info.cashReference &&
            info.pinReference &&
            info.internetReference &&
            info.url &&
            info.password &&
            info.username &&
            info.token
        )
    ) {
        console.log("no till info: " + bar + " - " + JSON.stringify(info))
        results.push("no till info: " + bar + " - " + JSON.stringify(info));
        throw new Error({ results: results, status: 'failed' });
    }
    var orders = await getValuesForDates(bar, info, start).then(a => a).catch(e => {
        console.error('Error getting ordeers: ', e);
        return null;
    });

    if (!orders) {
        results.push('no orders');
    }

    var stock = await getStock(admin, bar)
        .then((a) => a)
        .catch((e) => {
            return null;
        })

    var drinks = {};
    Object.values(stock).forEach(type => {
        Object.values(type).forEach(drink => {
            drinks[drink.serverId] = drink;
        })
    })
    var shots = {};

    try {
        orders.actualPaidOrders.forEach(async (order) => {
            order.orders.forEach(async (o) => {
                var shouldPush = false;
                o.items.forEach(async (item) => {
                    var dri = drinks[item.articleId];
                    if (dri && dri.tillCategory && dri.tillCategory.includes('Shotjes') && item.price != 0) {
                        shots[dri.serverId] = (shots[dri.serverId] || 0) + item.quantity;
                        if (bar == 'Groningen') {
                            shouldPush = true;

                        }
                    } else if (!dri) {
                        console.log('coudnt find drink: ' + item.articleId)
                    }
                })
            })
        })
    } catch (e) {
        console.error(e);
        results.push({ error: 'Error getting paid order shots ' });
    }

    orders.data.shots = shots;
    results.push(orders.data);
    await saveValues(orders.data);
    return results;


    async function saveValues(data) {
        const yesterday = new Date(data.yesterday);
        const barname = data.barname;

        var year = yesterday.getFullYear();
        var month = yesterday.getMonth();
        var day = yesterday.getDate();

        const ref = admin.database().ref("bars/" + barname + "/records/liveshots/" + year + "-" + month + "-" + day);

        try {
            await ref.child('shots').set(data.shots);
        } catch (e) {
            console.log('failed to save: shots');
        }

    }
};

exports.getLastHour = async (admin, bar) => {
    console.log('getting Last Hour for: ' + bar);
    var results = [];

    var start = new Date(); //now



    const info = await getTillInfo(admin, bar)
        .then((a) => a)
        .catch((e) => {
            return null;
        });

    if (!info ||
        !(
            info.cashReference &&
            info.pinReference &&
            info.internetReference &&
            info.url &&
            info.password &&
            info.username &&
            info.token
        )
    ) {
        console.log("no till info: " + bar + " - " + JSON.stringify(info))
        results.push("no till info: " + bar + " - " + JSON.stringify(info));
        throw new Error({ results: results, status: 'failed' });
    }
    var end = new Date(start);
    start.setHours(start.getHours() - 1);
    results.push("Getting: " + bar + " for date: " + start.toDateString() + " to " + end.toDateString());

    var orders = await getValuesForBetweenDates(bar, info, start, end).then(a => a).catch(e => {
        console.error('Error getting ordeers: ', e);
        return null;
    });

    console.log('orders: ' + bar, orders.length)
    if (!orders) {
        console.log('no orders');
        results.push('no orders');
    }



    try {
        var items = [];




        for (var report of orders.actualPaidOrders) {
            for (var order of report.orders) {

                for (var item of order.items) {
                    items.push({
                        time: new Date(order.dateTime).getTime(),
                        amount: numberTo2Decimals(item.singlePrice * item.quantity),
                        quantity: item.quantity,

                    });
                }
            }
        }

        var lastHour = new Date(start);

        var last15Minutes = new Date(start);
        last15Minutes.setMinutes(last15Minutes.getMinutes() + 45);

        var last5Minutes = new Date(start);
        last5Minutes.setMinutes(last5Minutes.getMinutes() + 55);

        var middle5Minutes = new Date(start);
        middle5Minutes.setMinutes(middle5Minutes.getMinutes() + 50);

        var first5Minutes = new Date(start);
        first5Minutes.setMinutes(first5Minutes.getMinutes() + 45);

        var data = {
            lastHour: 0,
            last15Minutes: 0,
            last5Minutes: 0,
            middle5Minutes: 0,
            first5Minutes: 0,
            individualOrders: items.length,
            barname: bar,

            start: start.getTime(),
            end: end.getTime(),
            key: "DuncanSaundersVerySecretKeyAJSDHSAJKDSKAJHD"
        }

        for (var item of items) {
            if (item.time > lastHour.getTime()) {
                data.lastHour += item.amount;
            }

            if (item.time > last15Minutes.getTime()) {
                data.last15Minutes += item.amount;
            }

            if (item.time > last5Minutes.getTime()) {
                data.last5Minutes += item.amount;
            }

            if (item.time > middle5Minutes.getTime()) {
                data.middle5Minutes += item.amount;
            }

            if (item.time > first5Minutes.getTime()) {
                data.first5Minutes += item.amount;
            }

        }



        console.log('got data so sending');

        results.push(data);
        await sendToCustomerServer(data);
    } catch (e) {
        console.error(e);
        results.push({ error: 'Error getting paid order shots ' });
    }



    return results;

};

function numberTo2Decimals(number) {
    return Math.round(Number(number) * 100) / 100;
}


async function sendToCustomerServer(data) {
    console.log("Sending to customer server: ", data);
    //url = "https://europe-west3-customerapp-e7bca.cloudfunctions.net/UpdateCreditPrice"
    var jsonData = JSON.stringify(data);
    const options = {
        hostname: 'europe-west3-customerapp-e7bca.cloudfunctions.net',
        path: '/UpdateCreditPrice',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': jsonData.length
        }
    };

    // Making the HTTP request
    const req = https.request(options, (res) => {
        let responseData = '';

        console.log(`statusCode: ${res.statusCode}`);

        res.on('data', (chunk) => {
            responseData += chunk;
        });

        res.on('end', () => {
            console.log('Response: ', responseData);
        });

        res.on('error', (e) => {
            bugReport(e);
            console.error('Error: ', e);
        });
    });

    req.on('error', (error) => {
        console.error(error);
        this.bugReport(error);
    });

    // Writing the JSON data to the request body
    req.write(jsonData);

    // Ending the request
    req.end();


    function bugReport(error) {
        const val = change.after.val();

        const payLoad = {
            data: {
                notification_type: "BugReport",
                ref: "BugReport",
                body: "Server reported from check last hour: " + error,
                title: "Last Hour Bug Report",
            },
            notification: {
                body: "Server reported from check last hour: " + error,
                title: "Last Hour Bug Report",
            },
            topic: "BugReport",
        };

        const options = {
            priority: "high",
        };

        return admin.messaging().send(payLoad, options);
    }

}
