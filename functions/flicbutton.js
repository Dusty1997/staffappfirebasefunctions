const { data } = require("jquery");
var atob = require('atob');
exports.handleFlicRequest = async (admin, req, res) => {
    console.log('Received flic request', req);

    // assert that the req has a serialNumber a clickType and optionally a batteryPercentage
    if (!req.serialNumber || !req.clickType) {
        return res.status(400).send('Bad request');
    }

    // get the device with the serialNumber
    var device = await admin.firestore().collection('devices').doc(req.serialNumber).get();

    // if the device does not exist
    // create a new device with the serialNumber
    if (!device.exists) {
        console.log('Device does not exist, creating new device');
        await admin.firestore().collection('devices').doc(req.serialNumber).set({
            serialNumber: req.serialNumber,
            clicks: 0,
            batteryPercentage: req.battery || 0,
            bar: "",
            name: "",
            description: "",
            alerts: []
        });

        // get the device again
        device = await admin.firestore().collection('devices').doc(req.serialNumber).get();
    }

    // get the device data
    device = device.data();
    device.batteryPercentage = req.battery || device.batteryPercentage;

    // add the click to the device
    device.clicks += 1;
    device.lastBatteryStatus = req.battery > 20 ? 'Good' : 'Low';
    console.log('updating device', device);
    await admin.firestore().collection('devices').doc(req.serialNumber).update(device);


    //update battery
    // register the click in firebase databsae under /devices/clicks/{timestamp}
    await admin.database().ref('devices/clicks/' + req.serialNumber).push({
        serialNumber: req.serialNumber,
        clickType: req.clickType,
        timestamp: new Date().getTime(),
        battery: req.battery || 0
    });

    await handleAlerts(device, admin);

    if (device.bar) {
        var body = "";
        var title = "";
        var vibrationTimings = [];
        try {
            switch (req.clickType) {
                case 'click':
                    title = 'Stand At Door';
                    body = 'Staff member requested to stand at the door';
                    vibrationTimings = [1000, 1000, 1000];
                    break;
                case 'double_click':
                    title = 'Service Request';
                    body = 'Staff member requested to collect glasses';
                    vibrationTimings = [1000, 1000, 1000, 1000, 1000];
                    break;
                case 'hold':
                    title = 'Security Door';
                    body = 'Go to the door immediately';
                    vibrationTimings = [1000, 4000, 1000, 4000];
                    break;
            }
            const payLoad = {
                data: {
                    bar_name: device.bar,
                    notification_type: "sensorevent_" + device.bar,
                    body,
                    title,
                    custom_vibration: "true",
                    "default_vibration_timings": "true",
                    "vibration_timings": JSON.stringify(vibrationTimings.map(a => (a / 1000) + "s")),
                },


                topic: "sensorevent_" + device.bar,
            };
            const options = {
                priority: "high",
            };
            admin.messaging().send(payLoad);
        } catch (e) {
            console.log("error sndig ntification", e);
        }
    }




    return res.status(200).send('OK');
};



async function handleAlerts(device, admin) {
    // get the alerts
    var alerts = device.alerts;

    // if there are no alerts return
    if (!alerts) {
        return;
    }

    console.log('Checking alerts', alerts);
}

exports.handleDraginoRequest = async (admin, req, res) => {

    console.log('Received Dragino request');
    var decoded = {};
    try {
        decoded = decodeUplink(req);
    } catch (error) {
        console.error('Error decoding Dragino request', error);
        return res.status(400).send('Bad request');
    }

    console.log('Decoded Dragino request', decoded);

    // get the device with the serialNumber
    var device = await admin.firestore().collection('devices').doc(decoded.deviceId).get();

    // if the device does not exist
    // create a new device with the serialNumber
    if (!device.exists) {
        console.log('Device does not exist, creating new device');

        await admin.firestore().collection('devices').doc(decoded.deviceId).set({
            serialNumber: decoded.deviceId,
            clicks: 0,
            batteryPercentage: decoded.BatV / 3.7 * 100,
            bar: "",
            name: "",
            description: "",
            alerts: []
        });

        // get the device again
        device = await admin.firestore().collection('devices').doc(decoded.deviceId).get();
    }

    // get the device data
    device = device.data();

    // add the click to the device
    device.clicks += 1;

    //update battery
    device.batteryPercentage = decoded.BatV / 3.7 * 100;

    if (decoded.DoorStatus) {
        device.lastDoorStatus = decoded.DoorStatus;
    }

    if (decoded.TempC_SHT) {
        device.lastTemperature = decoded.TempC_SHT;
    }

    if (decoded.Hum_SHT) {
        device.lastHumidity = decoded.Hum_SHT;
    }

    if (decoded.BatS) {
        device.lastBatteryStatus = decoded.BatS;
    }

    if (decoded.ILL_lux) {
        device.lastIllumination = decoded.ILL_lux;
    }

    if (decoded.BatV) {
        device.lastBatteryVoltage = decoded.BatV;
    }



    console.log('updating device', device);
    await admin.firestore().collection('devices').doc(decoded.deviceId).update(device);



    // register the click in firebase databsae under /devices/clicks/{timestamp}
    await admin.database().ref('devices/clicks/' + decoded.deviceId).push({
        serialNumber: decoded.deviceId,
        clickType: 'Dragino',
        timestamp: new Date().getTime(),
        battery: decoded.BatV / 3.7 * 100,
        data: decoded
    });

    await handleAlerts(device, admin);

    return res.status(200).send('OK');


    function decodeUplink(input) {
        var data = Object.assign({}, Decode(input.fPort, input.data));
        data.time = input.time;
        data.deviceId = input.deviceInfo.devEui;

        // get a version with no undefined values
        data = JSON.parse(JSON.stringify(data));
        return data;
    }


    function Decode(fPort, base64Payload) {
        const bytes = Uint8Array.from(atob(base64Payload), c => c.charCodeAt(0));
        if (fPort != 2) {
            throw new Error('This decoder only supports fPort 2');
        }

        const batteryRaw = (bytes[0] << 8) | bytes[1]; // Combine the two bytes
        const batStatusBits = (batteryRaw >> 14) & 0b11; // Extract bits 15:14
        const batVoltageRaw = batteryRaw & 0x3FFF; // Extract bits 13:0 (actual voltage in mV)
        const batVoltage = batVoltageRaw / 1000; // Convert to volts

        let batStatus;
        switch (batStatusBits) {
            case 0b00:
                batStatus = "Ultra Low (BAT <= 2.50V)";
                break;
            case 0b01:
                batStatus = "Low (2.50V < BAT <= 2.55V)";
                break;
            case 0b10:
                batStatus = "OK (2.55V < BAT <= 2.65V)";
                break;
            case 0b11:
                batStatus = "Good (BAT > 2.65V)";
                break;
            default:
                batStatus = "Unknown";
        }
        var data = {
            //External sensor
            Ext_sensor:
                {
                    "0": "No external sensor",
                    "1": "Temperature Sensor",
                    "4": "Interrupt Sensor send",
                    "5": "Illumination Sensor",
                    "6": "ADC Sensor",
                    "7": "Interrupt Sensor count",
                }[bytes[6]],

            //Battery,units:V
            BatV: batVoltage, // Battery in mV

            BatS: batStatus,

            //SHT20,temperature,units:
            TempC_SHT: ((bytes[2] << 8) | bytes[3]) / 100, // Temperature in °C

            //SHT20,Humidity,units:%
            Hum_SHT: ((bytes[4] << 8) | bytes[5]) / 100, // Humidity in %

            //DS18B20,temperature,units:
            TempC_DS:
                {
                    "1": ((bytes[7] << 24 >> 16 | bytes[8]) / 100).toFixed(2),
                }[bytes[6] & 0xFF],

            //Exti pin level,PA4
            Exti_pin_level:
                {
                    "4": bytes[7] ? "High" : "Low",
                }[bytes[6] & 0x7F],

            DoorStatus: {
                "4": bytes[7] ? "Closed" : "Open",
            }[bytes[6] & 0x7F],

            //Exit pin status,PA4
            Exti_status:
                {
                    "4": bytes[8] ? "True" : "False",
                }[bytes[6] & 0x7F],

            //BH1750,illumination,units:lux
            ILL_lux:
                {
                    "5": bytes[7] << 8 | bytes[8],
                }[bytes[6] & 0x7F],

            //ADC,PA4,units:V
            ADC_V:
                {
                    "6": (bytes[7] << 8 | bytes[8]) / 1000,
                }[bytes[6] & 0x7F],

            //Exti count,PA4,units:times
            Exit_count:
                {
                    "7": bytes[7] << 8 | bytes[8],
                }[bytes[6] & 0x7F],

        };
        return data;
    }
};