/* eslint-disable no-trailing-spaces */
/* eslint-disable padded-blocks */
/* eslint-disable no-undef */
/* eslint-disable object-curly-spacing */
/* eslint-disable indent */
/* eslint-disable no-var */
/* eslint-disable max-len */
/* eslint-disable guard-for-in */
/* eslint-disable prefer-const */
/* eslint-disable require-jsdoc */
/* eslint-disable prefer-promise-reject-errors */

const events = require("events");

const http = require('http');
const { mergeWith, cloneDeep, isNull, isNumber, isBoolean, merge, concat, reduce } = require("lodash");



/**
 * 
 * @param {*} admin 
 * @param {string} bar
 * @param {Date} date last day of month
 * @param {string} user 
 * @param {number} paid 
 */
exports.setPaid = async(admin, bar, date, user, paid) => {

    console.log(date.toISOString() + " " + user + " " + paid + " " + bar);
    if (date.getDate() != 21 && paid > 0) {
        return "will only set 'real paid' for payslip for 21st of monthm, edit full real paid there, and 0 for the retification!"
    }

    if (paid == 0) {
        return "will only set 'real paid' for payslip for 21st of month && real paid > 0!"
    }

    const start = new Date(date.getFullYear(), date.getMonth() - 1, date.getDate() + 1, 7, 0, 0, 0);
    const end = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 7, 0, 0, 0);

    var hours = [];

    const d = new Date(start);
    while (d <= end) {
        console.log('getting hours: ' + d.toISOString() + " - " + bar);
        const day = await getHours(bar, d, user).then(a => a).catch(e => {
            console.error(e);
            return [];
        })



        hours = concat(hours, day);


        d.setDate(d.getDate() + 1);
    }

    console.log(JSON.stringify(hours));

    const totalHours = hours.reduce((prev, a) => {

        prev.totalHours += a.total;
        prev.totalCost += a.total * (a.price || 0);
        return prev;
    }, { totalHours: 0, totalCost: 0 });

    console.log('real Hours', paid);
    console.log(totalHours);

    const difference = paid - totalHours.totalCost;
    const perHour = difference / totalHours.totalHours;

    console.log(difference, 'adding ' + perHour + ' per hour');
    hours.forEach(h => {
        h.actualCost = (h.total * (h.price || 0)) + h.total * perHour;
    })

    console.log(JSON.stringify(hours))

    for (var i = 0; i < hours.length; i++) {
        const res = await saveHours(bar, hours[i].date, hours[i].key, hours[i].actualCost);
        console.log('saving hours' + hours[i].date + ", " + hours[i].actualCost + ", " + JSON.stringify(res));

    }

    console.log('finised')
    return JSON.stringify(hours)





    async function getHours(bar, date, key) {
        return new Promise((res, rej) => {
            admin
                .database()
                .ref(
                    "bars/" +
                    bar +
                    "/hours/" +
                    date.getFullYear() +
                    "/" +
                    date.getMonth() +
                    "/" +
                    date.getDate()
                )
                .once("value", (val) => {
                    var list = [];
                    if (val) {
                        val.forEach((user) => {
                            const u = user.val();

                            if (u.key == key) {
                                list.push({
                                    key: u.key,
                                    total: u.total,
                                    name: u.name,
                                    price: u.price || 0,
                                    date: date.getFullYear() +
                                        "/" +
                                        date.getMonth() +
                                        "/" +
                                        date.getDate()
                                });
                            }
                            // console.log('adding user hours', u.key, u.total);

                        });
                    }


                    res(list)
                });
        })


    }

    async function saveHours(bar, date, key, cost) {
        return new Promise((res, rej) => {
            admin
                .database()
                .ref(
                    "bars/" +
                    bar +
                    "/hours/" +
                    date + "/" + key + "/actualCost"
                ).set(cost).then(a => {
                    res(a);
                }).catch(e => {
                    rej(e);
                });
        })


    }

}