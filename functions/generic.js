const events = require("events");

const http = require('http');
const { mergeWith, cloneDeep, isNull, isNumber, isBoolean, merge } = require("lodash");


const soaper = require("theendsoaper");
const Type = require("theendsoaper/modules/function-types");

exports.getTillInfo = async (admin, barname) => {
    return new Promise((res, rej) => {
        admin
            .database()
            .ref("bars/" + barname + "/tillInformation")
            .once("value", (snap) => {
                // console.log("tillinfo for: " + barname + ", info: ", snap.val());
                if (snap.val() != null) {
                    return res(snap.val())
                }

                return rej('no info');

            });
    })

}

exports.getStock = async (admin, barname) => {
    return new Promise((res, rej) => {
        admin
            .database()
            .ref("bars/" + barname + "/stock")
            .once("value", (snap) => {
                // console.log("tillinfo for: " + barname + ", info: ", snap.val());
                if (snap.val() != null) {
                    return res(snap.val())
                }

                return rej('no info');

            });
    })

}

exports.getValuesForDates = async (bar, info, start) => {
    if (!(info.cashReference &&
        info.pinReference &&
        info.internetReference &&
        info.url &&
        info.password &&
        info.username &&
        info.token)) {
        return TypeError('Info must have all its variables');
    }

    // console.log("Got Info for: ", bar, info);
    let cash = 0;
    let pin = 0;
    let extra = 0;
    let missing = 0;
    let other = 0;
    let staffPin = [];
    var vats = [];
    var actualPaidOrders = [];
    var yesterday = new Date(start);
    var today = new Date(start);

    var tables = {};

    quantities = {
        cash: 0,
        pin: 0,
        internet: 0,
        staff: 0,
        other: 0,
        missing: 0
    }

    yesterday.setHours(7);
    yesterday.setMinutes(31);
    yesterday.setSeconds(0, 0);

    today.setDate(today.getDate() + 1);
    today.setHours(7);
    today.setMinutes(30);
    today.setSeconds(59, 999);

    console.log("yesteday: " + yesterday.toISOString() + ", today: " + today.toISOString());

    return await soaper
        .detailedTurnoverReport(
            info.username,
            info.password,
            info.url,
            new Type().getDetailedTurnoverReport(),
            yesterday.toISOString().split("Z")[0],
            today.toISOString().split("Z")[0],
            info.token
        )
        .then((values2) => {

            if (Array.isArray(values2)) {
                values2.forEach((a) => {
                    if (a.bills.length === 0) {
                        // print.print(a.orders);
                    }
                    var table = String(a.tableNumber);
                    if (!tables[table]) {
                        tables[table] = {
                            name: String(a.tableNumber),
                            total: 0,
                            orders: 0,
                            opened: a.openDateTime,
                            closed: a.closeDateTime
                        }
                    }

                    tables[table].closed = a.closeDateTime;

                    a.orders.forEach((b) => {
                        //console.log(a);
                        //throw new Error('done');
                        b.items.forEach(o => {
                            var data = { drink: o.articleId, quantity: o.quantity, singlePrice: o.singlePrice, total: o.price, discount: o.discount, time: b.dateTime, vat: o.vatPercent, vatPaid: o.vat };
                            tables[table].orders += o.quantity;
                            vats.push(data);
                        })


                    })

                    a.bills.forEach((b) => {

                        b.payments.forEach((c) => {
                            tables[table].total += c.amount;
                            if (c.paymentId === info.pinReference) {
                                quantities.pin++;
                                pin += c.amount;
                                actualPaidOrders.push(a);
                            } else if (c.paymentId === info.cashReference) {
                                quantities.cash++;
                                cash += c.amount;
                                actualPaidOrders.push(a);
                            } else if (c.paymentId === info.internetReference) {
                                extra += c.amount;
                                quantities.internet++;
                                actualPaidOrders.push(a);
                            } else if (c.paymentId === info.staffReference) {
                                staffPin.push(a.orders);
                                quantities.staff++;
                            } else if (c.paymentId === info.missingReference) {
                                console.log("Missing: ", c.paymentId, info.missingReference);
                                missing += c.amount;
                                quantities.missing++;
                                actualPaidOrders.push(a);
                            } else {
                                other += c.amount;
                                quantities.other++;
                                console.log("Payments that aren't defined: ", c.paymentId);
                            }
                        });
                    });



                });
            } else {
                console.log("Values2 is not an array: ", values2);
                return new Error('Values2 is not an array');
            }
            return {

                data: {
                    pin: pin,
                    cash: cash,
                    extra: extra,
                    staffDrinks: staffPin,
                    quantities: quantities,

                    other: other,
                    yesterday: yesterday,
                    barname: bar,
                    tables: tables,
                },
                vats: vats,
                actualPaidOrders
            }
        }).catch((e) => {
            console.error("error in getValuesForDates: ", e);

            return new Error(e);
        })



}

exports.getValuesForBetweenDates = async (bar, info, start, end) => {
    if (!(info.cashReference &&
        info.pinReference &&
        info.internetReference &&
        info.url &&
        info.password &&
        info.username &&
        info.token)) {
        return TypeError('Info must have all its variables');
    }

    // console.log("Got Info for: ", bar, info);
    let cash = 0;
    let pin = 0;
    let extra = 0;
    let other = 0;
    let staffPin = [];
    var vats = [];
    var actualPaidOrders = [];
    var yesterday = new Date(start);
    var today = new Date(end);

    var tables = {};

    quantities = {
        cash: 0,
        pin: 0,
        internet: 0,
        staff: 0,
        other: 0
    }



    console.log("yesteday: " + yesterday.toISOString() + ", today: " + today.toISOString());

    return await soaper
        .detailedTurnoverReport(
            info.username,
            info.password,
            info.url,
            new Type().getDetailedTurnoverReport(),
            yesterday.toISOString().split("Z")[0],
            today.toISOString().split("Z")[0],
            info.token
        )
        .then((values2) => {


            if (Array.isArray(values2)) {

                values2.forEach((a) => {
                    if (a.bills.length === 0) {
                        // print.print(a.orders);
                    }
                    var table = String(a.tableNumber);
                    if (!tables[table]) {
                        tables[table] = {
                            name: String(a.tableNumber),
                            total: 0,
                            orders: 0,
                            opened: a.openDateTime,
                            closed: a.closeDateTime
                        }
                    }

                    tables[table].closed = a.closeDateTime;

                    a.orders.forEach((b) => {
                        //console.log(a);
                        //throw new Error('done');
                        b.items.forEach(o => {
                            var data = { drink: o.articleId, quantity: o.quantity, singlePrice: o.singlePrice, total: o.price, discount: o.discount, time: b.dateTime, vat: o.vatPercent, vatPaid: o.vat };
                            tables[table].orders += o.quantity;
                            vats.push(data);
                        })


                    })

                    a.bills.forEach((b) => {

                        b.payments.forEach((c) => {
                            tables[table].total += c.amount;
                            if (c.paymentId === info.pinReference) {
                                quantities.pin++;
                                pin += c.amount;
                                actualPaidOrders.push(a);
                            } else if (c.paymentId === info.cashReference) {
                                quantities.cash++;
                                cash += c.amount;
                                actualPaidOrders.push(a);
                            } else if (c.paymentId === info.internetReference) {
                                extra += c.amount;
                                quantities.internet++;
                                actualPaidOrders.push(a);
                            } else if (c.paymentId === info.staffReference) {
                                staffPin.push(a.orders);
                                quantities.staff++;
                            } else {
                                other += c.amount;
                                quantities.other++;
                                console.log("Payments that aren't defined: ", c.paymentId);
                            }
                        });
                    });



                });
            }

            return {

                data: {
                    pin: pin,
                    cash: cash,
                    extra: extra,
                    staffDrinks: staffPin,
                    quantities: quantities,

                    other: other,
                    yesterday: yesterday,
                    barname: bar,
                    tables: tables,
                },
                vats: vats,
                actualPaidOrders
            }
        }).catch((e) => {
            console.error(JSON.stringify(e));
            return new Error(e);
        })



}