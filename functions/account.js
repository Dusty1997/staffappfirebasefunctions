

exports.linkStaff = (admin, uid) => {
    getUser(admin,uid).then((val) => {
        const user = val.val();
        console.log(user);
        if(user.phoneType === 'android'){
            linkGoogle(user);
        } else if (user.phoneType === 'ios'){
            linkApple(user);
        } else {
            linkGoogle(user);
            linkApple(user);
        }
    })
    
};


exports.unlinkStaff = (admin,uid) => {
    getUser(admin,uid).then((val) => {
        const user = val.val();
        if(user.phoneType === 'android'){
            unlinkGoogle(user);
        } else if (user.phoneType === 'ios'){
            unlinkApple(user);
        }
    });
};

function getUser(admin,uid) {
    return admin.database().ref("staff/" + uid).once("value");
}

function linkGoogle(user) {

}

function linkApple(user) {

}

function unlinkGoogle(user){

}

function unlinkApple(user){

}