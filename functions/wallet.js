
const crypto = require("crypto")
var fs = require('fs');
const algorithm = 'aes-256-ctr';
const secretKey = crypto.createHash('sha256').update(String('asd90safh3rasfp983yjha;skjd')).digest('base64').substr(0, 32);
const iv = crypto.randomBytes(16);




exports.generateQR = (event, date) => {
    const cipher = crypto.createCipheriv(algorithm, secretKey, iv);

    const encrypted = Buffer.concat([cipher.update(event), cipher.final()]);

    return {
        iv: iv.toString('hex'),
        content: encrypted.toString('hex')
    };
}

exports.validateQR = (event) => {
    console.log("next event,", event);
    if (event === null || event.toString() === null) {
        return null;
    }
    let splits = event.toString().split('___');

    if (splits[2] && splits[2] === "customerCredits") {
        console.log('customer credits')
    }

    const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(splits[0], 'hex'));

    const decrpyted = Buffer.concat([decipher.update(Buffer.from(splits[1], 'hex')), decipher.final()]);

    return decrpyted.toString()

}

exports.orderDrink = (data, admin) => {
    console.log("data", data);

    var drinks = [];

    var username;
    var url;
    var password;

    var uid = data.uid;
    for (var key in data.order) {
        console.log(key, data.order[key]);
        var s = data.order[key].split("_");
        console.log(s);
        if (s.length < 3) {
            console.log("not enough arguements");
            return "Arguement Error"
        }
        drinks.push({ id: key, price: Number(s[1]), quantity: Number(s[0]), name: s[2] })
    }

    var receipt = data.QRReceipt | "";


    return getTillInfo();
    async function getTillInfo() {
        const snapshot = await admin.database().ref("bars/" + data.name + "/tillInformation").once("value");
        if (snapshot && snapshot.val()) {
            username = String(snapshot.child("username").val());
            url = String(snapshot.child("url").val());
            password = String(snapshot.child("password").val());

            console.log("Completion: Gotten Till Info");
            return getKey();
        } else {
            return "null";
        }
    }






    //Tells the database the location it is at
    async function getKey() {
        var info = { bar: data.name, drinks: {} };

        var total = 0;
        console.log(drinks);
        for (var i = 0; i < drinks.length; i++) {
            console.log("drinks", drinks[i]);
            total += drinks[i].price * drinks[i].quantity;
            info.drinks[drinks[i].id] = { price: drinks[i].price, quantity: drinks[i].quantity, name: drinks[i].name };
        }
        info.total = total;
        info.type = 'debit';
        info.receipt = receipt;

        if (data && data.customerOrder) {
            const snapshot = await admin.database().ref("customers/" + uid + "/credits").once("value");
            if (snapshot && snapshot.val()) {
                console.log('balance:', snapshot.val(), total);
                admin.database().ref("customers/" + uid + "/credits").set((Number(snapshot.val()) - total));


                admin.database().ref("customers/" + uid + "/history/" + new Date().getTime()).set(info);

                return sendOrder();
            } else {
                return "null balance??"
            }
        } else {

            const snapshot = await admin.database().ref("staff/" + uid + "/balance").once("value");
            if (snapshot && snapshot.val()) {
                console.log('balance:', snapshot.val(), total);
                admin.database().ref("staff/" + uid + "/balance/").set((Number(snapshot.val()) - total));


                admin.database().ref("stafforders/" + uid + "/" + new Date().getTime()).set(info);

                return sendOrder();
            } else {
                return "null balance??"
            }
        }




    }

    function sendOrder() {
        console.log("balance changed")
        return "success";
    }


}

