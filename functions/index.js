/* Name: index.js
  Owner: Duncan Saunders - The End Karaoke
  description: Firebase functions programs which include gathering drink stock quantities from the database,
    creating and getting tasks from asana and more.
  Usage: From source terminal run firebase deploy --only functions to upload to firebase */

const wallet = require("./wallet");
const Till = require("./till");
const Staff = require("./staff");
const CCV = require('./ccv')
const Email = require('./email')
//Packages and init
const functions = require("firebase-functions");
var admin = require("firebase-admin");
admin.initializeApp();
const rp = require("request-promise");
const https = require("https");
const crypto = require('crypto');
const Flic = require('./flicbutton');

exports.StaffOrder = functions
    .region("europe-west3")
    .https.onCall((data, context) => {
        try {
            return wallet.orderDrink(data, admin);
        } catch (error) {
            return error;
        }
    });

oneday = 60 * 60 * 24 * 1000;
exports.GenerateQR = functions
    .region("europe-west3")
    .https.onCall((data, context) => {
        console.log("called");
        //todo encrypt with private key
        var d = new Date();
        admin
            .database()
            .ref()
            .child("keys/" + data.text)
            .set(d.getTime());

        return wallet.generateQR(data.text + "---" + d.getTime());
    });

exports.HttpIntercept = functions
    .region("europe-west3")
    .https.onCall((req, res) => {
        console.log("starting");

        try {
            console.log("Http intercept starting: " + req, JSON.stringify(req));
            if (req === undefined) {
                console.log("Http intercept: prefail - no data");
                return "error no data";
            }
            let url = new URL(req.data.path);
            console.log(url, url.hostname);
            var options = {
                hostname: url.hostname,
                port: url.port,
                path: url.path,
                method: req.data.method,
                headers: req.data.headers,
                uri: url,
                params: req.data.params,
                withCredentials: req.data.withCredentials,
            };
            console.log(options);

            console.log("calling:", options);
            return rp(options)
                .then((body) => {
                    return body;
                })
                .catch((err) => {
                    return "rperror: " + err;
                });
        } catch (error) {
            return "error" + error;
        }
    });
exports.SoapIntercept = functions
    .region("europe-west3")
    .https.onCall((req, res) => {
        console.log("starting", req);
        return new Promise((resolve, reject) => {
            try {
                let data = "";
                https.get(req, (reso) => {
                    reso.on("data", (d) => {
                        console.log(d);
                        data += d;
                    });
                    reso.on("end", (req2) => {
                        return resolve(data);
                    });
                });
            } catch (error) {
                return reject(error);
            }
        });
    });


exports.ValidateQR = functions
    .region("europe-west3")
    .https.onCall((data, context) => {
        console.log("called");
        //todo encrypt with private key
        var result = wallet.validateQR(data.text);
        var splits = result.toString().split("---");
        if (!splits || splits.length == 1) {
            splits = result.toString().split('___');
        }
        async function validate() {
            console.log('decrypted', result);


            const then = await admin
                .database()
                .ref("keys/" + splits[0])
                .once("value");
            console.log(new Date().getTime() - Number(splits[1]), then.val());
            if (
                then &&
                then.val() &&
                then.val().toString() === splits[1] &&
                new Date().getTime() - Number(splits[1]) <= 1000 * 60 * 5
            ) {
                console.log("valid", then.val(), splits[1]);
                admin
                    .database()
                    .ref("keys/" + splits[0])
                    .set(null);
                console.log("returned:", splits[0]);
                return splits[0];
            } else {
                return null;
            }


        }

        async function customerValidate() {
            const uid = splits[0];
            const time = Number(splits[1]);
            console.log('validating customer', uid, time);
            return { uid: splits[0], type: 'customerapp' };
        }

        console.log(splits, splits[2], splits[2] == 'customerCredits')
        if (splits && splits[2] && splits[2] == 'customerCredits') {
            return customerValidate();
        } else {
            //staff app payment
            return validate();
        }


    });

exports.addStaff = functions
    .region("europe-west3")
    .https.onCall((data, context) => {
        //console.log("sent data",data.staff);
        const staff = data.staff;

        if (!staff) {
            return { status: 'failed', error: 'no staff data' };
        }



        return admin
            .auth()
            .createUser({
                email: staff.email,
                emailVerified: true,
                password: "asda76*&^*&AS6da87sd",
                displayName: staff.firstName,
                disabled: false,
            })
            .then((userRecord) => {
                console.log("made uidL", userRecord.uid);
                try {
                    const payLoad = {
                        data: {
                            bar_name: String(staff.bar),
                            notification_type: "staffadded",
                            user: userRecord.uid,
                            ref: userRecord.uid,
                            body: "Staff member added: " + staff.firstName,
                            title: staff.bar + " has a new staff member",
                        },

                        notification: {
                            body: "Staff member added: " + staff.firstName,
                            title: staff.bar + " has a new staff member",
                        },
                        topic: "staffadded",
                    };
                    const options = {
                        priority: "high",
                    };
                    admin.messaging().send(payLoad);
                } catch (e) {
                    console.log("error notifying amy", e);
                }

                return { status: 'success', data: userRecord.uid };
            })
            .catch((error) => {
                console.log('create user error: ', error);
                return { status: 'failed', error: error.toString(), data: null };
            });

    });


exports.sendEmail = functions.region("europe-west3").https.onCall(async (data, context) => {

    if (!data) {
        throw new Error('no data');
    }

    return await Email.sendEmail(data);
});

exports.sendEmail2 = functions.region("europe-west3").https.onRequest(async (data, context) => {

    context.set('Access-Control-Allow-Origin', '*');
    context.set('Access-Control-Allow-Methods', 'GET');
    console.log(data);
    if (!data) {
        throw new Error('no data');
    }

    return await Email.sendEmail(data);
});




exports.onStaffDelete = functions
    .region("europe-west3")
    .database.ref("/staff/{uid}")
    .onDelete((change, context) => {
        console.log("DeletedStaff", change.val());
        admin.auth().deleteUser(context.params.uid);
        return null;
    });

//eoc 11am on the 1st and 16th of the month
exports.CheckContracts = functions
    .region("europe-west3")
    .pubsub.schedule("5 11 1,16 * *")
    .timeZone("Europe/Amsterdam")
    .onRun((context) => {
        console.log("checking contracts");
        checkContracts().then((a) => {
            console.log("done checking contracts");
            return a;
        }).catch((e) => {
            console.log("error checking contracts", e);
            return e;
        });
    });

async function checkContracts() {
    return await admin
        .database()
        .ref()
        .child("staff")
        .once("value", async (snapshot) => {
            const date = new Date();
            var unique = [];
            var contractsEnding = [];
            snapshot.forEach((s) => {
                const staff = s.val();
                //time between now and the end of contract is 2 months
                contractsEnding.push(new Date(staff.eoc || 0).toDateString());
                if (

                    !staff.eoc || (Number(staff.eoc) - date.getTime() < 2 * 2629800000)

                ) {

                    if (!staff.bar || staff.bar == 'Pertempto' || staff.bar == 'Old Staff' || staff.bar == 'Daga Beheer') {
                        return;
                    }
                    unique.push(staff);
                }
            });



            console.log("notifications needed for", unique.length);
            //   console.log("contracts ending", contractsEnding);
            for (var b of unique) {
                const payload = {
                    data: {

                        notification_type: "EOC",
                        user: "",
                        ref: "EOC",
                        title: b.firstName + " " + b.lastName + " contract ends soon from " + b.bar,
                        body: "Contract ends soon from " + b.bar,
                    },
                    notification: {
                        title: b.firstName + " " + b.lastName + " contract ends soon from " + b.bar,
                        body: "Contract ends soon from " + b.bar,
                    },
                    topic: payload.data.notification_type,
                };
                const options = {
                    priority: "high",
                    title: b.firstName + " " + b.lastName + " contract ends soon from " + b.bar,
                    body: "Contract ends soon from " + b.bar,
                };

                console.log("sending to:", "EOC");
                await admin.messaging().send(payLoad);

            }

            return unique.length;
            //clear duplicates
        });
}
exports.runCheckContracts = functions.region("europe-west3").https.onCall((data, context) => {
    console.log("checking contracts");
    checkContracts().then((a) => {
        console.log("done checking contracts");
        return a;
    }).catch((e) => {
        console.log("error checking contracts", e);
        return e;
    });
});
//maintenance (status changed, or new user)
exports.WatchMaintenance = functions
    .region("europe-west3")
    .database.ref("bars/{barname}/tasks/{key}")
    .onWrite((change, context) => {
        //if it was deleted we ignore
        if (!change.after.exists()) {
            return null;
        }
        //if didnt previously exist or if the status has changed
        if (!change.before.exists() ||
            change.after.data().status !== change.before.data().status ||
            change.after.data().user !== change.before.data().user ||
            change.after.data().messages != change.before.data().messages
        ) {
            const payLoad = {
                data: {
                    bar_name: String(context.params.barname),
                    notification_type: "MaintenanceTask",
                    user: change.after.data().user,
                    ref: String(context.params.key),
                },
                topic: context.params.barname + "_" + payLoad.data.notification_type,
            };
            const options = {
                priority: "high",
            };

            return admin
                .messaging()
                .send(

                    payLoad,
                    options
                );
        }
        return "failed";
    });

//nd order (status changed)
exports.WatchNDOrders = functions
    .region("europe-west3")
    .database.ref("bars/{barname}/ndorders/{key}")
    .onWrite((change, context) => {
        //if it was deleted we ignore
        if (!change.after.exists()) {
            return null;
        }
        try {
            //if didnt previously exist or if the status has changed
            if (!change.before.exists() ||
                change.after.data().status !== change.before.data().status ||
                change.after.data().comments != change.before.data().comments
            ) {
                const payLoad = {
                    data: {
                        bar_name: String(context.params.barname),
                        notification_type: "NDOrderStatus",
                        user: "",
                        ref: String(context.params.key),
                    },
                    topic: context.params.barname + "_" + payLoad.data.notification_type,
                };
                const options = {
                    priority: "high",
                };
                return admin
                    .messaging()
                    .send(

                        payLoad,
                        options
                    );

            }
            return "failed";
        } catch (e) { }
    });

//order (status changed)
exports.WatchOrders = functions
    .region("europe-west3")
    .database.ref("bars/{barname}/orders/{key}")
    .onWrite((change, context) => {
        //if it was deleted we ignore
        if (!change.after.exists()) {
            return null;
        }
        //if didnt previously exist or if the status has changed
        if (!change.before.exists() ||
            change.after.data().status !== change.before.data().status ||
            change.after.data().comments != change.before.data().comments
        ) {
            const payLoad = {
                data: {
                    bar_name: String(context.params.barname),
                    notification_type: "OrderStatus",
                    user: "",
                    ref: String(context.params.key),
                },
                topic: context.params.barname + "_" + payLoad.data.notification_type,
            };
            const options = {
                priority: "high",
            };
            return admin
                .messaging()
                .send(

                    payLoad,
                    options
                );

        }
        return "failed";
    });

//watch telling messages
exports.WatchTellingMessages = functions
    .region("europe-west3")
    .database.ref("messages/{key}")
    .onUpdate((change, context) => {
        //if it was deleted we ignore
        if (!change.after.exists()) {
            return null;
        }
        const beforeData = change.before.val(); // Data before the write
        const afterData = change.after.val(); // Data after the write

        console.log(JSON.stringify(beforeData, null, 2), JSON.stringify(afterData, null, 2));
        // Check if data previously existed or if the status has changed
        if (!beforeData || afterData.lastMessage > beforeData.lastMessage) {

            const payload = {
                data: {
                    bar_name: String(context.params.barname),
                    notification_type: "Telling_Message",
                    user: "",
                    ref: "Telling_Message",
                    title: afterData.key,
                    body: String(context.params.key)
                },
                notification: {
                    title: afterData.key,
                    body: String(context.params.key)
                },
                topic: "Telling_Message",
            };
            const options = {
                priority: "high",
                title: afterData.key,
                body: String(context.params.key)
            };

            return admin
                .messaging()
                .send(
                    payload,
                    options
                );
        }
        return "failed";
    });
async function sendNotification(before, after, barname, date) {
    try {
        //get uids that have changed
        //removed or added
        var removed = [];
        var added = [];

        const staffB = before.staff ? before.staff : [];
        const staffA = after.staff ? after.staff : [];
        console.log(
            date,
            new Date(Number(date)),
            new Date(Number(date)).toDateString()
        );
        console.log(JSON.stringify(staffB), JSON.stringify(staffA));
        staffA.forEach((s) => {
            console.log(staffB.indexOf[s]);
            if (staffB.indexOf[s] == undefined) {
                added.push(s);
            }
        });

        staffB.forEach((s) => {
            if (staffA.indexOf[s] == undefined) {
                removed.push(s);
            }
        });

        const d = new Date(Number(date));
        d.setTime(d.getTime() + 1 * 60 * 1000 * 60);

        console.log(
            "added:" + JSON.stringify(added),
            "removed: " + JSON.stringify(removed),
            d,
            d.toDateString()
        );
        for (const s of removed) {
            const payLoad = {
                data: {
                    bar_name: String(barname),
                    notification_type: "shifts_" + s,
                    user: s,
                    ref: String(d.getTime()),
                    body: "Your scheduled shift on " + d.toDateString() + " has been removed",
                    title: "Shift Cancelled",
                },

                notification: {
                    body: "Your scheduled shift on " + d.toDateString() + " has been removed",
                    title: "Shift Cancelled",
                },
                topic: "shifts_" + s,
            };
            const options = {
                priority: "high",
            };
            await admin
                .messaging()
                .send(

                    payLoad,
                    options
                );

        }

        for (const s of added) {
            const payLoad = {
                data: {
                    bar_name: String(barname),
                    notification_type: "shifts_" + s,
                    user: s,
                    ref: String(d.getTime()),
                    body: "You have been scheduled on the " + d.toDateString() + " to work",
                    title: "Shift Added",
                },
                notification: {
                    body: "You have been scheduled on the " + d.toDateString() + " to work",
                    title: "Shift Added",
                },
                topic: "shifts_" + s,
            };
            const options = {
                priority: "high",
            };

            await admin.messaging().send(payLoad);
        }
    } catch (e) {
        return e;
    }

    return "success";
}

exports.WatchBalance = functions
    .region("europe-west3")
    .database.ref("staff/{uid}/balance")
    .onWrite((change, context) => {
        const before = change.before.val() ? change.before.val() : 0;
        const after = change.after.val();
        const uid = context.params.uid;
        console.log(before, after, uid);

        if (after && before) {
            if (after > before) {
                //credit
                var payLoad = {
                    data: {
                        user: uid,
                        notification_type: "balance_" + uid,
                        body: "€" +
                            (Number(after) - Number(before)).toFixed(2) +
                            " has been credited",
                        title: "Account Credited",
                    },

                    notification: {
                        body: "€" +
                            (Number(after) - Number(before)).toFixed(2) +
                            " has been credited",
                        title: "Account Credited",
                    },
                    topic: "balancecredit_" + uid,
                };
                const options = {
                    priority: "high",
                };

                return admin.messaging().send(payLoad);
            } else if (after < before) {
                //debit
                var payLoad = {
                    data: {
                        user: uid,
                        notification_type: "balance_" + uid,
                        body: "€" +
                            (Number(after) - Number(before)).toFixed(2) +
                            " has been debited",
                        title: "Account Dedited",
                    },

                    notification: {
                        body: "€" +
                            (Number(after) - Number(before)).toFixed(2) +
                            " has been debited",
                        title: "Account Dedited",
                    },
                    topic: "balancedebit_" + uid,
                };
                const options = {
                    priority: "high",
                };

                return admin.messaging().send(payLoad);
            }
        }
    });


exports.triggerSurveyNotification = functions
    .region("europe-west3")
    .https.onCall((context, res) => {

        if (context.users && context.users.length) {
            for (var user of context.users) {
                console.log(user);
                var bar = user.id
                const payLoad = {
                    data: {

                        notification_type: bar + "_NewSurvey2",
                        body: context.message,
                        title: context.title,
                    },

                    notification: {
                        body: context.message,
                        title: context.title,
                    },
                    topic: bar + "_NewSurvey2",
                };
                const options = {
                    priority: "high",
                };
                console.log(bar + ' notification sent')
                admin.messaging().send(payLoad);
            }
        } else if (context.bar && context.bar != '') {
            var bar = context.bar;
            const payLoad = {
                data: {

                    notification_type: bar + "_NewSurvey",
                    body: context.message,
                    title: context.title,
                },

                notification: {
                    body: context.message,
                    title: context.title,
                },
                topic: bar.replace(/ /g, "-") + "_NewSurvey"
            };
            const options = {
                priority: "high",
            };
            console.log(context.bar + ' notification sent')
            admin.messaging().send(payLoad);
        } else {
            const payLoad = {
                data: {

                    notification_type: "NewSurvey",
                    body: context.message,
                    title: context.title,
                },

                notification: {
                    body: context.message,
                    title: context.title,
                },
                topic: "NewSurvey",
            };
            const options = {
                priority: "high",
            };
            console.log('General notification sent')
            admin.messaging().send(payLoad);
        }

    });


exports.TriggerSchedule = functions
    .region("europe-west3")
    .https.onCall((context, res) => {
        const bar = context.bar;
        const start = new Date(context.start); //dateTime
        const staff = JSON.parse(context.staff);

        staff.forEach((s) => {
            const payLoad = {
                data: {
                    bar_name: String(bar),
                    notification_type: "shifts_" + s,
                    user: s,
                    ref: String(start.getTime()),
                    body: "Schedule for week starting " +
                        start.toDateString() +
                        " has been set!",
                    title: "Schedule Made",
                },

                notification: {
                    body: "Schedule for week starting " +
                        start.toDateString() +
                        " has been set!",
                    title: "Schedule Made",
                },
                topic: "shifts_" + s,
            };
            const options = {
                priority: "high",
            };

            admin.messaging().send(payLoad);
        });
    });
//incorrect Drinks

exports.WatchStock = functions
    .region("europe-west3")
    .database.ref("bars/{barname}/records/stock/{year}/{month}/{day}/start")
    .onWrite((change, context) => {
        //if it was deleted we ignore
        if (!change.after.exists()) {
            console.log(
                "start on " +
                String(context.params.year) +
                "/" +
                String(Number(context.params.month) + 1) +
                "/" +
                String(Number(context.params.day)) +
                " deleted"
            );
            return null;
        }

        if (change.before.exists()) {
            //already existed
            console.log(
                "start on " +
                String(context.params.year) +
                "/" +
                String(Number(context.params.month) + 1) +
                "/" +
                String(Number(context.params.day)) +
                " already exists"
            );
            return null;
        }

        const length = change.after
            .val()
            .filter((a) => a.difference && a.difference != 0).length;
        console.log(length);
        if (length > 0) {
            console.log(
                "differences: " + context.params.barname + "_" + "IncorrectDrinks"
            );

            const payLoad = {
                data: {
                    bar_name: String(context.params.barname),
                    notification_type: "IncorrectDrinks",
                    ref: "IncorrectDrinks",
                    body: context.params.barname +
                        " has " +
                        String(length) +
                        " incorrections",
                    title: "Incorrect Drinks",
                },
                notification: {
                    body: context.params.barname +
                        " has " +
                        String(length) +
                        " incorrections",
                    title: "Incorrect Drinks",
                },
                topic: context.params.barname + "_IncorrectDrinks",
            };

            const options = {
                priority: "high",
            };

            console.log(
                "notification for data: ",
                payLoad,
                context.params.barname + "_IncorrectDrinks"
            );

            return admin.messaging().send(payLoad);
        } else {
            console.log("no differences");
            return;
        }
    });

exports.triggerNotification = functions
    .region("europe-west3")
    .https.onCall((context, res) => {
        console.log(context.bar, context.type);
        const payLoad = {
            data: {
                bar_name: context.bar,
                notification_type: context.bar + "_" + context.type,
                user: "",
                body: context.body || "",
                title: context.title || "",
            },
            notification: {
                body: context.body || "",
                title: context.title || "",
            },
            topic: context.bar + "_" + context.type,
        };
        const options = {
            priority: "high",
        };
        console.log("Notification: " + context.bar + "_" + context.type);
        return admin.messaging().send(payLoad);

    });

exports.scheduleCountTill = functions
    .runWith({
        // Ensure the function has enough memory and time
        // to process large files
        timeoutSeconds: 300,
    })
    .region("europe-west3")
    .pubsub.schedule("20 8 * * *")
    .timeZone("Europe/Amsterdam")
    .onRun((context) => {
        if (typeof Promise.allSettled === 'function') {
            console.log('Promise.allSettled is supported');
        } else {
            console.log('Promise.allSettled is not supported');
            Promise.allSettled = function (promises) {
                return Promise.all(promises.map(promise => Promise.resolve(promise).then(value => { return value }, reason => ({
                    status: 'rejected',
                    reason,
                }))));
            };
        }
        return new Promise((resolve, reject) => {
            admin
                .database()
                .ref("barnames")
                .once("value", (snap) => {
                    //get values for each bar
                    try {
                        var infos = [];
                        snap.forEach((barname) => {
                            var bar = barname.val();
                            if (bar == "Pertempto") {
                                infos.push(new Promise((resolve, reject) => { resolve({ status: 'skipped', error: 'skipping Pertempto' }) }));
                                return;
                            }
                            var date = new Date();
                            date.setDate(date.getDate() - 1);
                            var info = Till.getInkoop(
                                admin,
                                barname.val(),
                                date.toLocaleString()
                            );

                            infos.push(info);
                        });


                        Promise.allSettled(infos).then((values) => { resolve(values) }).catch(e => { reject({ status: 'failed', error: e }) });



                    } catch (error) {
                        reject({ status: 'failed', error: error });
                    }
                });
        });
    });

exports.TestScheduleCountTill = functions
    .runWith({
        // Ensure the function has enough memory and time
        // to process large files
        timeoutSeconds: 300,
    })
    .region("europe-west3")
    .https.onCall((context, res) => {

        if (typeof Promise.allSettled === 'function') {
            console.log('Promise.allSettled is supported');
        } else {
            console.log('Promise.allSettled is not supported');
            Promise.allSettled = function (promises) {
                return Promise.all(promises.map(promise => Promise.resolve(promise).then(value => {

                    return value
                }, reason => {
                    console.log(reason);
                    return {
                        status: 'rejected',
                        reason,
                    }
                })));
            };
        }
        return new Promise((resolve, reject) => {
            admin
                .database()
                .ref("barnames")
                .once("value", (snap) => {
                    //get values for each bar
                    try {
                        var infos = [];
                        snap.forEach((barname) => {
                            var bar = barname.val();
                            if (bar == "Pertempto" || bar == 'Daga Beheer') {
                                infos.push(new Promise((resolve, reject) => { resolve({ status: 'skipped', error: 'skipping Pertempto' }) }));
                                return;
                            }

                            var date = new Date();
                            if (context.start) {
                                date = new Date(context.start);
                            } else {
                                date.setDate(date.getDate() - 1);
                            }

                            var info = Till.getInkoop(
                                admin,
                                barname.val(),
                                date.toLocaleString()
                            );

                            infos.push(info);
                        });


                        Promise.allSettled(infos).then((values) => { resolve(values) }).catch(e => { reject({ status: 'failed', error: e }) });



                    } catch (error) {
                        reject({ status: 'failed', error: error });
                    }
                });
        });
    });

exports.scheduleCountDiscounts = functions
    .region("europe-west3")
    .pubsub.schedule("30 8 * * *")
    .timeZone("Europe/Amsterdam")
    .onRun((context) => {
        if (typeof Promise.allSettled === 'function') {
            console.log('Promise.allSettled is supported');
        } else {
            console.log('Promise.allSettled is not supported');
            Promise.allSettled = function (promises) {
                return Promise.all(promises.map(promise => Promise.resolve(promise).then(value => {
                    console.log('value: ' + JSON.stringify(value));
                    return value
                }, reason => {
                    console.log(reason);
                    return {
                        status: 'rejected',
                        reason,
                    }
                })));
            };
        }
        return new Promise((resolve, reject) => {
            admin
                .database()
                .ref("barnames")
                .once("value", (snap) => {
                    //get values for each bar
                    try {
                        var infos = [];
                        snap.forEach((barname) => {
                            var bar = barname.val();
                            if (bar == "Pertempto") {
                                infos.push(new Promise((res, reject) => { res({ status: 'skipped', error: 'skipping Pertempto' }) }));
                                return;
                            }

                            if (bar == "Daga Beheer") {
                                infos.push(new Promise((res, reject) => { res({ status: 'skipped', error: 'skipping Daga Beheer' }) }));
                                return;
                            }
                            var date = new Date();
                            date.setDate(date.getDate() - 1);

                            console.log(bar, date.getTime(), new Date().getTime(), true);
                            const info = Till.getDiscounts(
                                admin,
                                bar,
                                date.getTime(),
                                new Date().getTime(),
                                true
                            )

                            infos.push(info);
                        });


                        Promise.allSettled(infos).then((values) => {
                            console.log('values: ' + JSON.stringify(values, null, 2));
                            return values;
                        }).catch(e => {
                            console.log('error: ' + JSON.stringify(e, null, 2));
                            reject({ status: 'failed', error: e })
                        });



                    } catch (error) {
                        reject({ status: 'failed', error: error });
                    }
                });
        });
    });

exports.clearManagerAllowances = functions
    .region("europe-west3")
    .https.onCall((context, res) => {
        admin
            .database()
            .ref("staff")
            .once("value", (val) => {
                val.forEach((u) => {
                    var user = u.val();
                    console.log(user);
                    if (user.uid && user.allowance) {
                        console.log(
                            "resetting allwoance for:",
                            user.uid + " to: " + user.allowance
                        );
                        admin
                            .database()
                            .ref("staff/" + user.uid + "/currentAllowance")
                            .set(user.allowance);
                    }
                });
            });
    });

exports.checkHours = functions
    .region("europe-west3")
    .pubsub.schedule("30 7 * * 0")
    .timeZone("Europe/Amsterdam")
    .onRun((context, res) => {
        const payLoad = {
            data: {
                body: "Review Staff Hours!",
                title: "Hours",
            },
            notification: {
                body: "Review Staff Hours!",
                title: "Hours",
            },
            topic: "CheckHours",
        };
        const options = {
            priority: "high",
        };
        return admin.messaging().send(payLoad);
    });

exports.ReviewHours = functions
    .region("europe-west3")
    .pubsub.schedule("30 7 21 * *")
    .timeZone("Europe/Amsterdam")
    .onRun((context, res) => {
        const payLoad = {
            data: {
                body: "Look over hours for full month: 21st -> 20th!",
                title: "Review Hours For Month",
            },
            notification: {
                body: "Look over hours for full month: 21st -> 20th!",
                title: "Review Hours For Month",
            },
            topic: "ReviewHours",
        };
        const options = {
            priority: "high",
        };
        return admin.messaging().send(payLoad);

    });

exports.triggerShifts = functions
    .region("europe-west3")
    .pubsub.schedule("30 9 * * 6")
    .timeZone("Europe/Amsterdam")
    .onRun((context, res) => {
        const payLoad = {
            notification: {
                body: "Set your shifts",
                title: "Set your shifts before Sunday!",
            },
            topic: "shifts",
        };
        const options = {
            priority: "high",
        };
        return admin.messaging().send(payLoad);
    });



exports.calculateStaffDrinks = functions
    .region("europe-west3")
    .pubsub.schedule("30 7 * * 2")
    .timeZone("Europe/Amsterdam")
    .onRun((context, res) => {
        return new Promise((c, r) => {
            //today tuesday 7:30am
            //-1 monday, -2 sun, -3 sat, -4 frid, -5thur -6wed -7tue -8mon -9sun

            const now = new Date();
            var endDate = new Date(
                now.getFullYear(),
                now.getMonth(),
                now.getDate() - 3,
                7,
                0,
                0,
                1
            );
            var startDate = new Date(
                now.getFullYear(),
                now.getMonth(),
                now.getDate() - 9,
                7,
                0,
                0,
                0
            );

            var promises = [];

            admin
                .database()
                .ref("barnames")
                .once("value", (ref) => {
                    ref.forEach((bar) => {
                        const name = bar.val();
                        console.log("running for :" + name);
                        promises.push(
                            doBar({
                                barname: name,
                                start: startDate,
                                end: endDate,
                                fake: false,
                                force: false,
                            })
                        );
                    });

                    Promise.all(promises)
                        .then((a) => {
                            console.log("all done", a);
                            return c(a);
                        })
                        .catch((e) => {
                            console.log("all failed?", e);
                            return r();
                        });
                });
        });

        async function doBar(obj) {
            var res;
            try {
                res = await Till.calculateStaffDrinks(admin, obj)
                    .then((a) => a)
                    .catch((e) => e);
            } catch (error) {
                res = error;
            }

            console.log(obj.barname + " finished :" + res);

            return res;
        }
    });

exports.TestStaffDrinks2 = functions
    .region("europe-west3")
    .https.onCall((context, res) => {
        /*
         */
        console.log(context);

        return new Promise((c, r) => {
            //today monday 7am
            //-1 sun, -2 sat, -3 frid, -4thur -5wed -6tue -7mon -8sun
            if (!context.bar) {
                return r("no bar");
            }
            const now = new Date();
            var endDate = new Date(context.end);
            var startDate = new Date(context.start);

            var promises = [];

            promises.push(
                doBar({
                    barname: context.bar,
                    start: startDate,
                    end: endDate,
                    fake: context.fake,
                    force: context.force,
                })
            );

            Promise.all(promises)
                .then((a) => {
                    console.log("all done", a);
                    return c(a);
                })
                .catch((e) => {
                    console.log("all failed?", e);
                    return r();
                });
        });

        async function doBar(obj) {
            var res;
            try {
                res = await Till.calculateStaffDrinks(admin, obj)
                    .then((a) => a)
                    .catch((e) => e);
            } catch (error) {
                res = error;
            }

            console.log(obj.barname + " finished :" + res);

            return { bar: obj.barname, data: res };
        }
    });

exports.GetDiscounts = functions
    .region("europe-west3")
    .https.onCall((context, res) => {
        return new Promise((resolve, reject) => {
            admin
                .database()
                .ref("barnames")
                .once("value", (snap) => {
                    //get values for each bar
                    console.log('context: ', context);
                    var bar = context.bar;

                    try {
                        var obj = {};
                        var infos = [];

                        var start = new Date();
                        start.setDate(start.getDate() - 1);
                        if (context.start) {
                            start = new Date(context.start);
                            start.setHours(7);
                            start.setMinutes(0);
                            start.setSeconds(0);
                            start.setMilliseconds(0);
                        }

                        var end = new Date(start);
                        end.setDate(end.getDate() + 1);
                        end.setHours(6);
                        end.setMinutes(59);
                        end.setSeconds(59);
                        end.setMilliseconds(999);

                        try {


                            console.log(bar, start.toISOString(), end.toISOString(), true);
                            const info = Till.getDiscounts(
                                admin,
                                bar,
                                start.getTime(),
                                end.getTime(),
                                true
                            )
                                .then((a) => {
                                    console.log("done", bar);
                                    obj[bar] = a;
                                    return { bar: bar, arr: a };
                                })
                                .catch((e) => {
                                    console.log("error getDiscounts done" + bar, e);
                                    return e;
                                });

                            console.log("pusing promise");
                            infos.push(info);
                        } catch (e) {
                            console.error("error adding promise: " + bar, e);
                        }

                        try {
                            console.log("waiting for all promises", infos.length);
                            Promise.all(infos)
                                .then((a) => {
                                    console.log("all promises done");
                                    return resolve(JSON.stringify(obj));
                                })
                                .catch((e) => reject("error"));
                        } catch (e) {
                            console.log("problem waiting for promises", e);
                        }
                    } catch (error) {
                        console.log("global error " + error);
                        resolve("global error " + error);
                    }
                });
        });


    });

async function getInfo(barname) {
    return new Promise((res, rej) => {
        admin
            .database()
            .ref("bars/" + barname + "/tillInformation")
            .once("value", (snap) => {
                // console.log("tillinfo for: " + barname + ", info: ", snap.val());
                if (snap && snap.val()) {
                    return res(snap.val());
                }
                return rej({});
            });
    });
}

exports.GetDetailedTurnover = functions
    .runWith({ timeoutSeconds: 120, memory: "1GB" })
    .region("europe-west3")
    .https.onCall(async (context, res) => {
        var date = new Date(context.month);
        const bar = context.bar;

        const info2 = await getInfo(bar)
            .then((a) => a)
            .catch((e) => {
                console.log("cant get info", bar, e);
                return null;
            });

        if (!info2) {
            return "no info :(";
        }


        //get values for each bar


        var infos = [];

        try {


            const info = Till.getDetailedTurnover(
                admin,
                bar,
                date.getTime(),
                new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1, 11, 30).getTime(),
                info2
            )
                .then((a) => {
                    console.log("done " + bar + " " + date.getTime());
                    return JSON.parse(a);
                })
                .catch((e) => {
                    console.log("done " + bar + " " + date.getTime(), e);
                    return e;
                });

            console.log("pusing promise");
            infos.push(info);
            date = new Date(
                date.getFullYear(),
                date.getMonth(),
                date.getDate() + 1
            );


            console.log("waiting for all promises", infos.length);
            return await Promise.all(infos)
                .then((a) => {
                    console.log("all promises done");
                    return a;
                })
                .catch((e) => JSON.stringify(e));
        } catch (e) {
            return JSON.stringify(e);
        }
    });

exports.InOutCashItem = functions
    .region("europe-west3")
    .https.onCall((context, res) => {
        return Till.monthlyBuild(admin, context.bar || null, context.date || null);
    });

exports.TestInkoop = functions
    .runWith({
        // Ensure the function has enough memory and time
        // to process large files
        timeoutSeconds: 300,
    })
    .region("europe-west3")
    .https.onCall((context, res) => {
        return Till.getInkoop(admin, context.bar, context.start);
    });

/* Not currently needed
exports.GetShots = functions.pubsub.schedule('every 15 minutes').onRun(async (context) => {
    console.log('This will be run every 5 minutes!');
    var date = new Date();
    if (date.getHours() > 18 || date.getHours() < 5) {
        console.log('not between 5 and 18')
        return;
    }
    var turnedOn = await new Promise((res, rej) => {
        admin.database().ref('functions/getshots').once('value', (snap) => {
            if (snap && snap.val()) {
                return res(snap.val());
            }
            return rej({});
        })
    });

    if (!turnedOn) {
        console.log('not turned on');
        return;
    }

    if (typeof Promise.allSettled === 'function') {
        console.log('Promise.allSettled is supported');
    } else {
        console.log('Promise.allSettled is not supported');
        Promise.allSettled = function (promises) {
            return Promise.all(promises.map(promise => Promise.resolve(promise).then(value => { return value }, reason => ({
                status: 'rejected',
                reason,
            }))));
        };
    }
    return new Promise((resolve, reject) => {
        admin
            .database()
            .ref("barnames")
            .once("value", (snap) => {
                //get values for each bar
                try {
                    var infos = [];
                    snap.forEach((barname) => {
                        var bar = barname.val();
                        if (bar == "Pertempto" || bar == "Daga Beheer") {
                            infos.push(new Promise((resolve, reject) => { resolve({ status: 'skipped', error: 'skipping Pertempto' }) }));
                            return;
                        }
                        var date = new Date();
                        date.setDate(date.getDate() - 1);
                        var info = Till.getShots(
                            admin,
                            barname.val(),
                            date.toLocaleString(), true
                        );

                        infos.push(info);
                    });


                    Promise.allSettled(infos).then((values) => { resolve(values) }).catch(e => { reject({ status: 'failed', error: e }) });



                } catch (error) {
                    reject({ status: 'failed', error: error });
                }
            });
    });
});
*/
exports.GetCustomerCreditPriceInformation = functions.pubsub.schedule('every 15 minutes').onRun(async (context) => {
    console.log('This will be run every 15 minutes!');
    var date = new Date();
    console.log('date: ', date.getHours());
    if (date.getHours() > 5 && date.getHours() < 16) {
        console.log('not between 4pm and 5am')
        return;
    }
    var turnedOn = await new Promise((res, rej) => {
        admin.database().ref('functions/getliveprice').once('value', (snap) => {
            if (snap && snap.val()) {
                return res(snap.val());
            }
            return rej({});
        })
    });

    if (!turnedOn) {
        console.log('not turned on');
        return;
    }

    if (typeof Promise.allSettled === 'function') {
        console.log('Promise.allSettled is supported');
    } else {
        console.log('Promise.allSettled is not supported');
        Promise.allSettled = function (promises) {
            return Promise.all(promises.map(promise => Promise.resolve(promise).then(value => { return value }, reason => ({
                status: 'rejected',
                reason,
            }))));
        };
    }
    return new Promise((resolve, reject) => {
        admin
            .database()
            .ref("barnames")
            .once("value", (snap) => {
                //get values for each bar
                try {
                    var infos = [];
                    snap.forEach((barname) => {
                        var bar = barname.val();
                        if (bar == "Pertempto" || bar == "Daga Beheer" || bar == "Old Staff") {
                            infos.push(new Promise((resolve, reject) => { resolve({ status: 'skipped', error: 'skipping Pertempto' }) }));
                            return;
                        }

                        var info = Till.getLastHour(
                            admin,
                            barname.val(),
                        );

                        infos.push(info);
                    });


                    Promise.allSettled(infos).then((values) => {
                        console.log('result', JSON.stringify(values));
                        resolve(values)
                    }).catch(e => {
                        console.log('failed', e);
                        reject({ status: 'failed', error: e })
                    });



                } catch (error) {
                    console.log('error', error);
                    reject({ status: 'failed', error: error });
                }
            });
    });
});

exports.monthlyBuild = functions
    .runWith({
        // Ensure the function has enough memory and time
        // to process large files
        timeoutSeconds: 300,
    })
    .region("europe-west3")
    .pubsub.schedule("0 9 15 * *")
    .timeZone("Europe/Amsterdam")
    .onRun((context, res) => {
        return Till.monthlyBuild(admin);
    });

exports.SetPaid = functions
    .region("europe-west3")
    .https.onCall((context, res) => {
        return Staff.setPaid(
            admin,
            context.bar,
            new Date(context.date),
            context.user,
            context.paid
        );
    });

exports.GetTillToday = functions
    .runWith({
        // Ensure the function has enough memory and time
        // to process large files
        timeoutSeconds: 300,
    })
    .region("europe-west3")
    .https.onCall(async (context, res) => {
        console.log("context: ", context);
        if (context.time && context.bar) {
            try {
                return await Till.getInkoop(admin, context.bar, context.time);
            } catch (error) {
                console.log('Error:' + error);
                return { reason: "Till inkoop error", error: error.message };
            }
        } else {
            console.log("missing args");
            return { reason: "ivalid arguements", error: new Error("Invalid Arguments") };
        }

        //get the bars

        //firebase function to get bars
    });

exports.WatchBugs = functions
    .region("europe-west3")
    .database.ref("bugReports/open/{date}")
    .onWrite((change, context) => {
        //if it was deleted we ignore
        if (!change.after.exists()) {
            return null;
        }

        if (change.before.exists()) {
            //already existed
            return null;
        }

        const val = change.after.val();

        const payLoad = {
            data: {
                notification_type: "BugReport",
                ref: "BugReport",
                body: val.user.name + " reported " + val.title,
                title: "Bug Report",
            },
            notification: {
                body: val.user.name + " reported " + val.title,
                title: "Bug Report",
            },
            topic: "BugReport",
        };

        const options = {
            priority: "high",
        };
        return admin.messaging().send(payLoad);
    });



exports.PinPayment = functions
    .region("europe-west3")
    .database.ref("bars/{bar}/records/ccv/{ms}")
    .onWrite(async (change, context) => {
        //if it was deleted we ignore
        if (!change.after.exists()) {
            return null;
        }
        const val = change.after.val();
        if (change.before.exists()) {
            return null;
        }


        const params = context.params;

        const result = await CCV.initiatePayment(admin, params, val);
        console.log("result :" + JSON.stringify(result, null, 2));
        return result;


    });

exports.ccvWebhook = functions.https.onRequest(async (request, response) => {

    const data = request.body;
    console.log('ccv webhook', JSON.stringify(data, null, 2));

    if (!data || !data.id) {
        return;
    }

    //there is an, however there is extra data =>  probably not correct webhook
    if (Object.values(data).length > 1) {
        return;
    }

    const info = await new Promise((res, rej) => {
        const cleanReference = data.id.replace('.', '-');
        admin
            .database()
            .ref("webhooks/ccv/" + cleanReference)
            .once("value", (snap) => {
                // console.log("tillinfo for: " + barname + ", info: ", snap.val());
                if (snap.val() != null) {
                    return res(snap.val())
                }

                return rej('no info');

            });
    }).then(a => a).catch(e => {
        console.error(e);
        return null;
    })

    if (!info) {
        return;
    }

    console.log('payment ' + info.id + " with ccv reference " + info.ccvId + " has been webhooked");
    await admin.database().ref("bars/" + info.bar + "/records/ccv/" + info.date + "/status").set('webhook');
    info.location = info.bar;
    const result = await CCV.checkPayment(admin, info);
    console.log("CCV Transaction Result: " + JSON.stringify(result, null, 2));

})



exports.requestPaymentStatus = functions.https.onCall(async (request, response) => {
    console.log(request);
    if (!request.id) {
        throw new Error('no id');
    }

    if (!request.bar) {
        throw new Error('no bar');
    }



    const payment = await admin.database().ref("bars/" + request.bar + "/records/ccv/" + request.id).once('value').then(a => a.val());
    console.log(payment)
    if (!payment) {
        throw new Error('payment not found');
    }

    if (!payment.ccvId) {
        throw new Error('payment not initiated');
    }

    console.log('getting payment status: ' + request.id);
    const result = await CCV.checkPayment(admin, payment);

    return result;



});

exports.WatchMaintenanceStatus = functions
    .region("europe-west3")
    .database.ref("bars/{bar}/tasks/{ref}")
    .onWrite(async (change, context) => {
        //if it was deleted we ignore
        if (!change.after.exists()) {
            return null;
        }
        var previous;
        if (change.before.exists()) {
            previous = change.before.val();
        }
        const next = change.after.val();

        if (next.status == 'removed') {
            return;
        }

        try {
            console.log("previous: " + JSON.stringify(previous, null, 2));
            console.log("next: " + JSON.stringify(next, null, 2));

        } catch (error) {
            console.log(previous);
            console.log(next);
        }

        var data = {
            notification_type: "MaintenanceTask",
            ref: "MaintenanceTask: " + context.params.ref,
            body: next.title + " has been ",
            title: "Maintenance Status Changed To ",
        };


        var reference = "";
        var newTask = false;
        if (!previous) {
            newTask = true;
            //task created
            console.log('task created');
            reference = "newMaintenanceTask";
            data.body = data.body + "Created at " + context.params.bar;
            data.title = data.title + "Created";
        } else if (previous.status === next.status) {
            console.log('status not changed');
            return;
        } else if (previous.status === 'archived' && next.status === 'requested') {
            //reopened
            console.log('reopened');
            reference = "reopenedMaintenanceTask";
            data.body = data.body + "reopened at " + context.params.bar;
            data.title = data.title + "reopened";
        } else if (previous.status != 'archived' && next.status == 'archived') {
            //task completed
            reference = context.params.bar + "_" + "doneMaintenanceTask";
            data.body = data.body + "completed";
            data.title = data.title + "Completed";
        }






        var notification = {
            body: data.body,
            title: data.title
        };
        const payLoad = {
            data,
            notification
        };

        const options = {
            priority: "high",
        };

        console.log('sending notification to ' + reference + " " + JSON.stringify(data, null, 2))
        if (newTask) {
            payLoad.topic = context.params.bar + "_" + reference;

        } else {
            payLoad.topic = reference;
        }
        return admin.messaging().send(payLoad);
    });


//write function for a customer to get their credits and history
exports.GetCustomerCredits = functions.region('europe-west3').https.onRequest(async (request, response) => {

    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Methods', 'GET');
    const info = request.query;

    console.log('info', info);
    if (!info.uid || info.uid == 'undefined') {
        return response.status(500).send('No Data');
    }
    //get credits and history
    const credits = await admin.database().ref("customers/" + info.uid + "/credits").once('value').then(a => a.val());
    const history = await admin.database().ref("customers/" + info.uid + "/history").once('value').then(a => a.val());

    console.log(credits, history);
    return response.status(200).send(JSON.stringify({ credits, history }));
});

exports.GetCustomerQR = functions.region('europe-west3').https.onRequest(async (request, response) => {


    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Methods', 'GET');
    const info = request.query;

    console.log('info', info);
    //info should contain the bar and the customer uid
    if (!info.uid || info.uid == 'undefined') {
        console.log('Customer QR: no bar or uid')
        return response.status(500).send('No Bar or UID');
    }


    //get wallet qr code
    const qrCode = wallet.generateQR(info.uid + "___" + Date.now() + "___customerCredits");

    var d = new Date();
    admin
        .database()
        .ref()
        .child("keys/" + info.uid)
        .set(d.getTime());

    return response.status(200).send(qrCode);
});

exports.onWorkingChanged = functions.region('europe-west3').database.ref("bars/{bar}/working/{year}/{month}/{date}/{uid}").onWrite(async (change, context) => {
    //get the new value
    const newValue = change.after.val();
    const oldValue = change.before.val();

    //example old or new {"{uid1}":"{uid1}", "{uid2}":"{uid2}"} or null


    const bar = context.params.bar;
    const date = context.params.year + "/" + context.params.month + "/" + context.params.date;
    const uid = context.params.uid;

    console.log('working changed', bar, date, newValue, oldValue);

    const now = Date.now();
    if (!newValue) {
        //staff unregistered as working
        const update = await admin.database().ref("bars/" + bar + "/workedRecords/" + date + "/" + uid + "/updates").push({ date: now, type: 'unregistered', uid, bar, shiftDate: date });
        console.log('update', { date: now, type: 'unregistered', uid, bar, shiftDate: date });
    }

    if (!oldValue) {
        //staff registered as working
        const update = await admin.database().ref("bars/" + bar + "/workedRecords/" + date + "/" + uid + "/updates").push({ date: now, type: 'registered', uid, bar, shiftDate: date });
        console.log('update', { date: now, type: 'registered', uid, bar, shiftDate: date });
    }

    return;

});

exports.OnCreditChanged = functions.region('europe-west3').database.ref("customers/{uid}/credits").onWrite(async (change, context) => {
    //get the new value
    const newValue = change.after.val();
    const oldValue = change.before.val();
    const uid = context.params.uid;

    console.log('credit changed', uid, newValue, oldValue);

    if (newValue == oldValue) {
        return;
    }

    //http send to customerdb the changes
    return new Promise((res, rej) => {
        try {

            https.get('https://europe-west3-customerapp-e7bca.cloudfunctions.net/CreditChangedCallRequest?uid=' + uid + "&credits=" + newValue, (resp) => {
                let data = '';
                resp.on('data', (chunk) => {
                    data += chunk;
                });
                resp.on('end', () => {
                    console.log('crediting account,data');

                    return res(data);
                });
            }).on("error", (err) => {

                console.log("Error: " + err.message);
                return rej(err);

            });

        } catch (e) {

            return rej(e)
        }

    });

});


exports.creditAccount = functions.region('europe-west3').https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Methods', 'GET');
    const data = request.query;
    if (!data) {
        console.log('CreditAccount: no data')
        return response.status(500).send('No Data');
    }

    console.log('credit account input', data.data);
    const encryptedData = data.data;
    const key = "asd90safh3rasfp983yjha;skjd";
    const info = JSON.parse(decrypt(encryptedData, key));
    console.log('decrypted:', info);

    if (!info || !info.uid || !info.amount) {
        console.log('no uid or amount');
        return response.status(500).send('No UID or Amount');
    }

    const credits = await admin.database().ref("customers/" + info.uid + "/credits").once('value').then(a => a.val());
    const newCredits = credits + info.amount;
    await admin.database().ref("customers/" + info.uid + "/credits").set(newCredits);

    const newHistory = { date: Date.now(), total: info.amount, type: 'credit', drinks: { 'credit': { quantity: 1, price: info.amount, name: "credit" } } };
    await admin.database().ref("customers/" + info.uid + "/history/" + newHistory.date).set(newHistory);

    return response.status(200).send('Success');

    function decrypt(data, key) {
        const algorithm = 'aes-256-ctr';

        console.log('decrypting', data, key)
        const secretKey2 = crypto.createHash('sha256').update(key).digest('base64').substr(0, 32);
        try {
            const decipher = crypto.createDecipheriv(algorithm, secretKey2, Buffer.from(data.split("___")[0], 'hex'));

            const decrpyted = Buffer.concat([decipher.update(Buffer.from(data.split("___")[1], 'hex')), decipher.final()]);

            return decrpyted.toString()
        } catch (error) {
            console.error('Decryption error:', error.message);
            return null;
        }
    }

})

exports.closeDyanmicVoucher = functions.region('europe-west3').https.onCall(async (data, context) => {

    if (!data || !data.bar || !data.key || !data.staff || !data.time) {
        console.log('CloseDynamicVoucher: no data')
        return { status: 'failed', error: 'No Data' }
    }

    console.log('close dynamic voucher input', data);

    const status = await admin.database().ref("dynamicvouchers/" + data.bar + "/" + data.key + "/" + data.staff + "/" + data.time).set('closed').then(a => a).catch(e => e);

    return { status: 'success', data: status };
});
//encrypt on call
exports.encryptVoucher = functions.region('europe-west3').https.onCall(async (data, context) => {
    const algorithm = 'aes-256-ctr';
    const secretKey = crypto.createHash('sha256').update(String('asd90safh3rasfp983yjha;skjd')).digest('base64').substr(0, 32);
    const iv = crypto.randomBytes(16);

    function encrypt(event) {
        const cipher = crypto.createCipheriv(algorithm, secretKey, iv);
        const encrypted = Buffer.concat([cipher.update(event), cipher.final()]);
        return iv.toString('hex') + '___' + encrypted.toString('hex');
    }

    //get the send informatin
    const info = data;
    console.log('encrypting', info);

    if (!info.bar) {
        //return error with message

        return { "status": "failed", "error": "no bar" }
    }

    if (!info.key) {
        return { "status": "failed", "error": "no key" }
    }

    const voucherInfo = await admin.database().ref("vouchers/" + info.bar + "/" + info.key).once('value').then(a => a.val());

    if (!voucherInfo) {
        return { "status": "failed", "error": "no voucher" }
    }

    var finalKey = voucherInfo.key;
    if (info.isDynamic) {

        if (!info.staff || !info.dynamicVoucherTime) {
            return { "status": "failed", "error": "no staff or time" }
        }
        var staff = info.staff || "unknown";
        var time = Number(info.dynamicVoucherTime);


        // so this voucher can be usedd multiple times and as such needs to be ammended to the key before encryption
        finalKey = voucherInfo.key + "___" + staff + "___" + info.dynamicVoucherTime;

        await admin.database().ref("dynamicvouchers/" + data.bar + "/" + voucherInfo.key + "/" + staff + "/" + time).set('open');
    }
    const encrypted = encrypt(finalKey);
    console.log('encrypted', encrypted);
    return { "status": "success", "data": encrypted }
});

exports.setVoucherUsage = functions.region('europe-west3').https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Methods', 'GET');


    const data = JSON.parse(request.query.data);
    if (!data || !data.bar || !data.key) {
        console.log('SetVoucherUsage: no data,', data)
        return response.status(500).send('No Data');
    }

    console.log('set voucher usage input', data);
    const bar = data.bar;
    const key = data.key;

    const voucherInfo = await admin.database().ref("vouchers/" + bar + "/" + key).once('value').then(a => a.val());

    if (!voucherInfo) {
        return response.status(500).send('No Voucher');
    }

    if (!isNaN(voucherInfo.usages)) {
        voucherInfo.usages += 1;
    } else {
        voucherInfo.usages = 1;
    }

    await admin.database().ref("vouchers/" + bar + "/" + key + "/usages").set(voucherInfo.usages);

    return response.status(200).send('Success:' + voucherInfo.usages);


})


//decrypt on request
exports.decryptVoucher = functions.region('europe-west3').https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Methods', 'GET');
    const data = request.query;
    if (!data || !data.data || !data.bar) {
        console.log('DecryptVoucher: no data')
        return response.status(500).send('No Data');
    }

    console.log('decrypt voucher input', data);
    const encryptedData = data.data;
    const key = "asd90safh3rasfp983yjha;skjd";
    const info = decrypt(encryptedData, key)
    console.log('decrypted:', info);

    if (!info) {
        console.log('no bar or key');
        return response.status(500).send('No Bar or Key');
    }

    //info is either a string or a string___string___string check
    var decryptedKey = info;
    var staff = null;
    var time = null;
    var status = null;
    if (info.split('___').length < 2) {
        //normal voucher
    } else {
        //dynamic voucher
        [decryptedKey, staff, time] = info.split('___');

        //check if the voucher is still valid
        status = await admin.database().ref("dynamicvouchers/" + data.bar + "/" + decryptedKey + "/" + staff + "/" + time).once('value').then(a => a.val()).catch(e => null)

        //is the time less than 20 minutes ago
        if (Number(time) + 1200000 < Date.now()) {
            console.log('time expired');
            status = 'closed';
            await admin.database().ref("dynamicvouchers/" + data.bar + "/" + decryptedKey + "/" + staff + "/" + time).set('closed');
        }
    }

    const voucherInfo = await admin.database().ref("vouchers/" + data.bar + "/" + decryptedKey).once('value').then(a => a.val());

    if (!voucherInfo) {
        return response.status(500).send('No Voucher');
    }

    voucherInfo.staff = staff;
    voucherInfo.time = time;
    voucherInfo.status = status;

    return response.status(200).send(voucherInfo);

    function decrypt(data, key) {
        const algorithm = 'aes-256-ctr';
        const [splits0, splits1, splits2] = data.split('___');
        console.log('decrypting', splits1, splits2, key)
        try {
            const secretKey = crypto.createHash('sha256').update(String('asd90safh3rasfp983yjha;skjd')).digest('base64').substr(0, 32);
            const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(splits1, 'hex'));

            const decrpyted = Buffer.concat([decipher.update(Buffer.from(splits2, 'hex')), decipher.final()]);

            return decrpyted.toString()
        } catch (error) {
            console.error('Decryption error:', error.message);
            return null;
        }

    }
});


exports.ComputerReporter = functions.region("europe-west3").https.onRequest(async (request, response) => {

    const data = request.body;
    if (!data) {
        console.log('Computer reporter: no data')
        return response.status(500).send('No Data');
    }

    console.log('computer reporter', JSON.stringify(data, null, 2));
    const encryptedData = data.data;
    const key = "aE5FWKAKFRrA4em1RQ6qDrviwXnNL32I";
    const info = decrypt(encryptedData, key);
    console.log(info);


    if (!info) {
        console.log('Computer reporter: no decrypted data')
    }

    if (!info.PC) {
        console.log('Computer reporter: no pc')
        return response.status(500).send('No PC');
    }

    if (!info.Bar) {
        console.log('Computer Reporter: no bar');
        return response.status(500).send('No Bar');
    }




    var history = (await admin.database().ref('computers/' + info.Bar + '/info/' + info.PC).once('value')).val();

    if (!history) {
        console.log('Computer reporter: no history')
        history = {
            InternalIP: null,
            ExternalIP: null,
            Bar: null,
            PC: null,
            AvailableRAM: null,
            TotalRAM: null,
            Time: null,
        }
    }

    console.log('checking ip addresses: ' + isIPv4Address(info.InternalIP) + ' ' + isIPv4Address(info.ExternalIP));


    if (info.InternalIP != history.InternalIP && isIPv4Address(info.InternalIP) && isIPv4Address(history.InternalIP)) {
        console.log("IP Changed!");
        try {
            const payLoad = {
                data: {
                    notification_type: "IPChange",
                    ref: "IPChange",
                    body: "Internal IP changed from " + history.InternalIP + " to " + info.InternalIP + " at " + info.Bar + " on PC " + info.PC,
                    title: "Internal IP Changed At " + info.Bar,
                },
                notification: {
                    body: "Internal IP changed from " + history.InternalIP + " to " + info.InternalIP + " at " + info.Bar + " on PC " + info.PC,
                    title: "Internal IP Changed At " + info.Bar,
                },
                topic: "IPChange",
            };

            const options = {
                priority: "high",
            };
            await admin.messaging().send(payLoad);
        } catch (e) {
            console.log(e)
        }

    }

    if (info.ExternalIP != history.ExternalIP && isIPv4Address(info.ExternalIP) && isIPv4Address(history.ExternalIP)) {
        console.log("IP Changed!");
        try {
            const payLoad = {
                data: {
                    notification_type: "IPChange",
                    ref: "IPChange",
                    body: "External IP changed from " + history.ExternalIP + " to " + info.ExternalIP + " at " + info.Bar + " on PC " + info.PC,
                    title: "External IP Changed At " + info.Bar,
                },
                notification: {
                    body: "External IP changed from " + history.ExternalIP + " to " + info.ExternalIP + " at " + info.Bar + " on PC " + info.PC,
                    title: "External IP Changed At " + info.Bar,
                },
                topic: "IPChange",
            };

            const options = {
                priority: "high",
            };
            await admin.messaging().send(payLoad);

        } catch (e) {
            console.log(e)
        }
    }

    const apps = Object.assign([], info.HeaviestApps);
    info.HeaviestApps = null;




    await admin.database().ref('computers/' + info.Bar + '/info/' + info.PC).set(info);
    await admin.database().ref('computers/' + info.Bar + '/usage/' + info.PC + '/' + info.Time).set(apps);

    return response.status(200).send('Function executed successfully');



    function isIPv4Address(input) {
        // Regular expression pattern for IPv4 address
        var ipv4Pattern = /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

        // Test the input against the pattern
        return ipv4Pattern.test(input);
    }




    function decrypt(encryptedData, key) {
        try {
            const decipher = crypto.createDecipheriv('aes-256-cbc', key, Buffer.alloc(16)); // Set a fixed IV of 16 bytes
            let decrypted = decipher.update(encryptedData, 'base64', 'utf8');
            decrypted += decipher.final('utf8');
            return JSON.parse(decrypted);
        } catch (error) {
            console.error('Decryption error:', error.message);
            return null;
        }
    }



})
const express = require("express");
var bodyParser = require('body-parser')
const cors = require('cors');
const app = express()
// configure app here, add routes, whatever
exports.api = functions.region('europe-west3').https.onRequest(app)
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    // try read the body or raw body
    next();
});

// Define the handler for the FlicButtonPressed route
app.put("/FlicButtonPressed", (req, res) => {
    console.log("Received Flic button press:", req.body, req.params);
    //is there content, raw body or anything?


    const { "serial-number": serialNumber, "click-type": clickType, "battery": battery } = req.body;

    // Validate the incoming data
    if (!serialNumber || !clickType) {
        return res.status(400).send({
            error: {
                message: "Bad Request: Missing serial-number or click-type",
                status: "INVALID_ARGUMENT",
            },
        });
    }

    // Process the data (add your business logic here)
    console.log("Received serial-number:", serialNumber);
    console.log("Received click-type:", clickType);

    // Send a response
    return Flic.handleFlicRequest(admin, { serialNumber, clickType, battery }, res)
});

//Define the handler for DraginoChirpstackMessage
app.post("/event", (req, res) => {
    console.log("Received Dragino Chirpstack Message:");

    if (req.body && req.body.fPort && req.body.fPort == 2) {
        console.log("Received Dragino Chirpstack Message:");
        return Flic.handleDraginoRequest(admin, req.body, res);
    }

    return res.status(400).send({
        error: {
            message: "Bad Request: Missing or wrong fPort",
            status: "INVALID_ARGUMENT",
        },
    });

    //is there content, raw body

});

//catch all other routes
app.all('*', (req, res) => {
    console.log('catch all', req.url);
    res.status(404).send('Not Found');
});