// main.js
var buttonManager = require("buttons");

var buttons = buttonManager.getButtons();

var http = require("http");
var firebaseUrl = "https://europe-west3-staffapp-b0578.cloudfunctions.net/api/FlicButtonPressed"; // Replace with your Firebase URL

// Array to store timestamps of the last clicks
var clickTimestamps = [];
var cooldown = false;

buttonManager.on("buttonSingleOrDoubleClickOrHold", function (obj) {
    var button = buttonManager.getButton(obj.bdaddr);
    var clickType = obj.isSingleClick ? "click" : obj.isDoubleClick ? "double_click" : "hold";
    var currentTime = Date.now();

    // Clean up timestamps older than 20 seconds
    clickTimestamps = clickTimestamps.filter(timestamp => currentTime - timestamp <= 20000);

    if (cooldown) {
        console.log('cooling down');
        return;
    }

    // If more than 5 clicks have been made in the last 20 seconds, ignore the current click for 1 minute
    if (clickTimestamps.length > 5) {
        if (!cooldown) {
            cooldown = true;
            console.log("Too many clicks! Ignoring clicks for 1 minute.");
            setTimeout(function () {
                cooldown = false;
                console.log("Cooldown over. Can process clicks again.");
            }, 60000); // 1 minute cooldown
        }
        return; // Ignore the current click
    }

    // Add the current click timestamp
    clickTimestamps.push(currentTime);

    console.log('Click Type: ' + clickType);
    var payload = JSON.stringify({
        "serial-number": button.serialNumber,
        "click-type": clickType,
        "timestamp": currentTime,
        "battery": button.batteryStatus
    });

    console.log(payload)


    // Send data to Firebase Realtime Database using HTTP request
    http.makeRequest({
        url: firebaseUrl,
        method: "PUT", // Use POST to add new data to Firebase
        headers: { "Content-Type": "application/json" },
        content: payload,

    }, function (err, res) {
        console.log("request status: ", res.content);
    });
});

console.log("Started");
